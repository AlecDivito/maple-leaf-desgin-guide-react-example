module.exports = {
    staticFileGlobs: [
        'build/**/*.js',
        'build/**/*.css',
        'build/**/media/*',
        'build/index.html'
    ],
    navigateFallback: '/index.html',
    navigateFallbackWhitelist: [/^\/(css)/],
    cacheId: 'mlfdesign-bypass'
}