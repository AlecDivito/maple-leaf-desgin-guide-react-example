import { Resolution } from '../models';

const IsMobile = (): boolean => {
  return document.documentElement.clientWidth < Resolution.TABLET;
}

export default IsMobile;