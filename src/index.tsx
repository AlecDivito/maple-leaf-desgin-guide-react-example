import * as React from 'react';
import * as ReactDOM from 'react-dom';
import registerServiceWorker from './registerServiceWorker';

import { routerMiddleware, connectRouter, ConnectedRouter } from 'connected-react-router';
import { applyMiddleware, compose, createStore, combineReducers } from 'redux';
import { createBrowserHistory } from 'history';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';

import { Route, Switch } from 'react-router-dom';
import Web, { meta as webmeta } from './contentComponents/web';
import Home, { meta as homemeta } from './contentComponents/home';
import Power, { meta as powermeta } from './contentComponents/power';
import AsyncComponent from './components/async/AsyncComponent';

import Nav from './components/nav/Nav';
import Swipe from './components/swipe/Swipe';
import Modal from './components/modal/Modal';
import { SWIPED_LEFT, SWIPED_RIGHT } from './redux/swipeActions';
import './index.css';

import { sidenavReducer } from './redux/sidenavReducers';
import { navReducer } from './redux/navReducers';
import { modalReducer } from './redux/modalReducers';
import { swipeReducer } from './redux/swipeReducers';

const history = createBrowserHistory({ basename: process.env.PUBLIC_URL });
const composeEnhancer = (window as any)._REDUX_DEVTOOLS_EXTENSION_COMPOSE_ || compose;
export const store = createStore(
  connectRouter(history)(
    combineReducers({
      nav: navReducer,
      sidenav: sidenavReducer,
      modal: modalReducer,
      swipe: swipeReducer
    })
  ),
  composeEnhancer(
    applyMiddleware(thunk, routerMiddleware(history))
  )
);

const Errorpage = () => import('./pages/Errorpage');
const Contentpage = () => import('./pages/Contentpage');
const ComingSoonpage = () => import('./pages/ComingSoonpage');
const Loader = (moduleProvider: () => Promise<any>, props?: object) => (
  () => <AsyncComponent moduleProvider={moduleProvider} props={props}/>
);
const SidenavAction = (type: string) => (dispatch: any, getState: () => any) => {
  const { nav, sidenav } = getState();
  dispatch({
    type,
    nav: nav.expanded,
    sidenav: sidenav.expanded
  });
};

export const ROUTES = [
  { name: "Home", url: "/", component: Contentpage, props: {items: Home, meta: homemeta } },
  { name: "Web", url: "/web", component: Contentpage, props: {items: Web, meta: webmeta } },
  { name: "PowerApps", url: "/power", component: Contentpage, props: {items: Power, meta: powermeta} },
  { name: "iOS", url: "/ios", component: ComingSoonpage },
  { name: "Android", url: "/android", component: ComingSoonpage }
];

ReactDOM.render(
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <div className="mlf__pages">
        <Nav/>
        <Switch>
          {
            ROUTES.map(({ url, component, props }, key) =>
              <Route key={key} exact path={url} component={Loader(component, props)}/>
            )
          }
          <Route component={ Loader(Errorpage) }/>
        </Switch>

        {/* Invisible or Shown on Trigger */}
        <Swipe LeftAction={SidenavAction(SWIPED_LEFT)} RightAction={SidenavAction(SWIPED_RIGHT)}/>
        <Modal/>
      </div>
    </ConnectedRouter>
  </Provider>, document.getElementById('root')
);
registerServiceWorker();