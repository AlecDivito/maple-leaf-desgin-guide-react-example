import * as React from 'react';

interface IProps {
  placeholder?: string;
  type?: string;
  id?: string;
  modifier?: string;
  prepend?: any;
  prependModifier?:string;
  append?: any;
  appendModifier?: string;
  alternativeInput?: any;
};

export class Input extends React.Component<IProps, any> {
  public render() {
    const { 
      type, 
      placeholder, 
      modifier, 
      prepend, 
      append, 
      id, 
      alternativeInput,
      prependModifier,
      appendModifier 
    } = this.props;
    return <div className={"input-group mlf__input" + (modifier? " mlf__input" + modifier: "")}>
    {
      prepend? 
      <div className={"input-group-prepend mlf__input--prepend" + (prependModifier? " mlf__input" + prependModifier: "")}>
        {
          typeof prepend === "string"? 
          <span className="input-group-text">{prepend}</span>
          : 
          prepend
        }
      </div>
      : null

    }
    {
      alternativeInput? 
        alternativeInput 
        : 
        <input
        type={type ? type : "text"}
        className={"form-control"}
        placeholder={placeholder ? placeholder : ""}
        id={id? id: ""}
    />
    }
    
    {
      append? 
      <div className={"input-group-append mlf__input--append"  + (appendModifier? " mlf__input" + appendModifier: "")}>
        {
          typeof append === "string"? 
          <span className="input-group-text">{append}</span>
          : 
          append
        }
      </div>
      : null
    }
  </div>
  }
};