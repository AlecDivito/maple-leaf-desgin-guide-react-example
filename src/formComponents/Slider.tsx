import * as React from 'react';

interface IProps {
  modifier?: string;
  min?: number;
  max?: number;
  step?: number;
  id?: string;
}
export class Slider extends React.Component<IProps, any> {
  public render() {
    let max = 100;
    let min = 0;
    let step = 1;

    this.props.max? max = this.props.max as number : max = 100;
    this.props.min? min = this.props.min as number : min = 0;
    this.props.step? step = this.props.step as number : step = 1;

    const val = (max + min)/2;

    return <React.Fragment>
        <input type="range" className={"custom-range mlf__slider" + (this.props.modifier? " mlf__slider--" + this.props.modifier : "")}
          id={this.props.id? this.props.id : ""}
          min={min}
          max={max}
          step={step}
          defaultValue={val.toString()}/>
      </React.Fragment>;
  }
};