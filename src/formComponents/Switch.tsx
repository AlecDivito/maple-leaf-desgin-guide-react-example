import * as React from 'react';

interface IProps {
  size?: number;
  modifier?: string;
  className?: string;
  items?: [any, any];
}
export class Switch extends React.Component<IProps> {

  public static defaultProps = {
    size: 22
  };

  public render() {
    const size = this.props.size as number;
    const { className, modifier, items } = this.props;
    return <div 
      className={ "input-group" + (className? " " + className: "") }
      style={ {height: size + "px", width: size*1.8 + "px"} }
    >
      <input className="mlf__switch__input" type="checkbox"/>
      <div className={ "mlf__switch" + (modifier? " mlf__switch" + modifier: "") }>
        <div className="mlf__switch__dot"/>
        {
          items? items.map((value, key) => {
            return <div className="mlf__switch__icon" style={{fontSize: size*2.6 + "%"}} key={key}>
              { value }
            </div>;
          }): null
        }
      </div>
    </div>;
  }
};