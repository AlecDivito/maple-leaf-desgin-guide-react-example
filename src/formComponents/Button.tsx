import * as React from 'react';

interface IProps {
  modifier?: string;
  text?: string;
  icon?: any;
  link?: any;
}
export class Button extends React.Component<IProps, any> {
  public render() {
    return <React.Fragment>
      <button 
        className={("mlf__button" + (this.props.modifier? " mlf__button--" + this.props.modifier: "") + " btn btn-default")} 
        type="submit" 
        onClick={() => ( this.props.link? window.location = this.props.link : "" )}>
          { this.props.icon === "chevron_left"? <i className="material-icons chev_left">{ this.props.icon }</i>: null }
          {(this.props.text? this.props.text : "")}
          { this.props.icon === "chevron_right"? <i className="material-icons chev_right">{ this.props.icon }</i>: null }
      </button>
    </React.Fragment>;
  }
};