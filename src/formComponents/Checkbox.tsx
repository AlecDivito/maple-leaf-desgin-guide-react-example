import * as React from 'react';

interface IProps {
    size?: number;
    modifier?: string;
    text?: string;
    id?: string;
    disabled?: string;
  }
  export class Checkbox extends React.Component<IProps, any> {
    public render() {
      const boxDisabled = this.props.disabled? true : false;

      return (
        <div className={"custom-checkbox mlf__checkbox" + (this.props.modifier? " mlf__checkbox--" + this.props.modifier : "")}>
          <input type="checkbox" className="custom-control-input mlf__checkbox__input" id={this.props.id} disabled={boxDisabled}/>
          <label className="custom-control-label mlf__checkbox__label" htmlFor={this.props.id}>
            {this.props.text? this.props.text : null}
          </label>
        </div>
      );
    }
  };