import * as React from 'react';
interface IProps {
  label?: string;
  modifier?: string;
  active?: any;
}
export class Radio extends React.Component<IProps, any> {
  public render() {
    const {label, modifier, active} = this.props;
    return <label className="mlf__radio">{label? label: null}
      <input 
        type="radio" 
        checked={active? active :false}
        className="mlf__radio__button"
        readOnly
      />
      <span className={"mlf__radio__dot" + (modifier? " mlf__radio" + modifier: "")}/>
    </label>
  }
};