import * as React from 'react';
import { UncontrolledTooltip, UncontrolledTooltipProps } from 'reactstrap';

export class Tooltip extends React.Component<UncontrolledTooltipProps> {

  public static defaultProps = {
    placement: "top"
  }

  public render() {
    const { target, placement, className, children } = this.props;
    return (
      <UncontrolledTooltip 
        placement={placement}
        className={"mlf__tooltip" + (className? " " + className: "")}
        target={target}
        delay={{show: 0, hide: 100}}
        autohide={false}
      >
        { children }
      </UncontrolledTooltip>
    );
  }
};