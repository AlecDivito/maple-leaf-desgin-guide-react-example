import * as React from 'react';
import { UncontrolledDropdown, DropdownToggle, DropdownMenu, DropdownItem, DropdownItemProps } from 'reactstrap';


interface IProps {
    items: Array<{props?: DropdownItemProps, content?: string}>;
    modifier?: string;
}

export class Dropdown extends React.Component<IProps> {

    public static defaultProps = {
        modifier: ""
    };

    public render() {
        const { children, items, modifier } = this.props;
        return <UncontrolledDropdown className={ "mlf__dropdown" + modifier}>
            <DropdownToggle caret>
                { children }
            </DropdownToggle>
            <DropdownMenu>
                {
                    items.map((item, key) => <DropdownItem key={key} { ...item.props }>
                        { item.content }
                    </DropdownItem>)
                }
            </DropdownMenu>
        </UncontrolledDropdown>
    }
}