import COLOR_1 from './MLF_Colour_1.jpg';
import COLOR_2 from './MLF_Colour_2.jpg';
import COLOR_3 from './MLF_Colour_3.jpg';
import COLOR_4 from './MLF_Colour_4.jpg';

export {
  COLOR_1,
  COLOR_2,
  COLOR_3,
  COLOR_4
}