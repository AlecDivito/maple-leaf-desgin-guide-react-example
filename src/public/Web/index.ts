import * as ANDROID from './android.jpg';
import * as IOS from './ios.jpg';
import * as WEB from './web.jpg';

export {
  ANDROID,
  IOS,
  WEB
}