import * as MODAL_LEFT from './modal__left.gif';
import * as MODAL_CENTER from './modal__center.jpg';
import * as MODAL_RIGHT from './modal__right.gif';

export {
  MODAL_LEFT,
  MODAL_CENTER,
  MODAL_RIGHT
}