import FRAME from './logo--frame.svg';
import PRIME from './logo--prime.jpg';
import PARTNER from './logo--partner.svg';
import BACK_1 from './back--one.jpg';
import BACK_2 from './back--two.jpg';
import TYPOGRAPHY_1 from './typography1.jpg';
import TYPOGRAPHY_2 from './typography2.jpg';

export {
  FRAME,
  PRIME,
  PARTNER,
  BACK_1,
  BACK_2,
  TYPOGRAPHY_1,
  TYPOGRAPHY_2
}