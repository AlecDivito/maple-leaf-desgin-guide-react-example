import * as React from 'react';
import { TYPOGRAPHY_1,TYPOGRAPHY_2 } from '../../public/Logo';
import Code from '../../components/code/Code';

export default class Typography extends React.Component {
  public render() {
    return <div className="content__item content__item--logo">
      <div className="mlf__card mlf-clear">
        <h2 className="content__header--first">Overview</h2>
        <p>Typography plays an important part in design as it can enhance, create, and alter the meaning and feelings of any verbal language - the content. It is important that typography provides legiblity, readability, appropriateness, pairing, and some tackness. Hence, <strong>we offer you the finest fonts the web has to offer</strong>... Google Fonts. Below would be the link tag for the required fonts.</p>
        <Code>
          <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto+Slab|Open+Sans"/>
        </Code>
      </div>
      <div className="mlf__card mlf-clear">
        <h2 className="content__header">Primary Typeface</h2>
        <div className="content__row--col-2 content__row">
          <div className="typo__row-item">
            <h3 className="typo__header typo__header--primary">Roboto Slab</h3>
            <p>Roboto Slab is used as headers (ie. &lt;h1&gt; ... &lt;h6&gt;) to provide emphasis on the page. However, <strong>do not</strong> use the primary typeface in all uppercase as it would be too destructive, instead opt to use a primary color instead.</p>
          </div>
          <div className="typo__row-item">
            { this.demo("typo__demo--primary") }
          </div>
        </div>
        <h2 className="content__header">Secondary Typeface</h2>
        <div className="content__row--col-2 content__row">
          <div className="typo__row-item">
            <h3 className="typo__header typo__header--primary">Open Sans</h3>
            <p>Open Sans is a general purpose font from paragraphs to subheaders such as capitions. To provide more emphasis consider using color, weight, and/or all caps (We recommmend using text-transform for all caps to help with accessiblity).</p>
          </div>
          <div className="typo__row-item">
          { this.demo() }
          </div>
        </div>
      </div>
      <div className="mlf__card mlf-clear">
        <h2 className="content__header">Typography in use</h2>
        <p style={{color: "#E8312D"}}><strong>The typography can be used in two ways.</strong></p>
        <ul style={{margin: "6px 0"}} className="content__list">
          <li>For hardworking pieces that need to communicate a lot of information, follow the guides for standard use.</li>
          <li>In pieces that are merely meant to communicate the brand and don’t require a lot of copy or information, you may follow the guides for expressive use - and have a little fun.</li>
        </ul>

        <div className="content__row--col-2 content__row">
          <div className="typo__row-item">
            <h3 className="typo__header typo__header--primary">Standard Use</h3>
            <img className="typo__img" src={TYPOGRAPHY_1}/>
            <p style={{color: "#E8312D"}}><strong>How-To</strong></p>
            <ul style={{margin: "6px 0"}}>
              <li>Text-align center, sentence case</li>
              <li>Include punctuation in headlines</li>
              <li>Use a smaller font-size for subheaders</li>
            </ul>
          </div>
          <div className="typo__row-item">
            <h3 className="typo__header typo__header--primary">Expressive Use</h3>
            <img className="typo__img" src={TYPOGRAPHY_2}/>
            <p style={{color: "#E8312D"}}><strong>How-To</strong></p>
            <ul style={{margin: "6px 0"}}>
              <li>Staggered/playful type (Main header only, ie. h1)</li>
              <li>Type may interact with/overlap food</li>
              <li>Headlines in sentence case or all lowercase</li>
              <li>Punctuation is optional in headlines</li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  }

  private demo(modifier?: string) {
    const TEXT = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789 !@#$%&?";
    return (
      <textarea className={"typo__demo" + (modifier? " " + modifier: "")} defaultValue={TEXT}/>
    );
  }
}