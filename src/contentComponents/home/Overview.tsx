import * as React from 'react';
import { WEB, IOS, ANDROID } from '../../public/Web';
import Code from '../../components/code/Code';
import SwipeCard from '../../components/card/SwipeCard';
import { Button, Input, Slider } from '../../formComponents';
 
const CARDS = [
  { title: "Web", body: "Use customized MLF styles for web applications including form controls, modals, typography and more.", img: WEB, link: "/web"},
  { title: "iOS", body: "This is a work in progress.", img: IOS, link: "/ios" },
  { title: "Android", body: "This is a work in progress.", img: ANDROID, link: "/android" }
  ];

const PRINCIPLES = [
  { number: "01.", principle: "Stand for honesty", body: "Nothing faked. Nothing idealized. No 'dressing up'. Let's show food and families as they are - we have nothing to hide."},
  { number: "02.", principle: "Be radically simple", body: "Simplicity feels clean and easy & reflects the food. Strive for simple, but not sterile."},
  { number: "03.", principle: "Humanize", body: "We are here to empower parents and families. Be approachable and open to everyone."}];

const EXAMPLES = [
  { component: <Input type="text" prepend="Name"/>, code: <div className="input-group mlf__input"><input type="text" className="form-control"/></div>},
  { component: <Button text="Button"/>, code: <button className="btn mlf__button" role="button" type="submit">Button</button>},
  { component: <Slider modifier="blue"/>, code: <input type="range" className="custom-range mlf__slider mlf__slider--blue"/>}
];


export default class Content extends React.Component {

  private CONTENT = [
    { header: "Multiple Platforms", subheader: "For all kinds of applications", factory: this.buildCards },
    { header: "Consistent Styles", subheader: "Following the principles of the MLF branding", factory: this.buildPrinciples },
    { header: "Easy Accessibility", subheader: "Simply copy-paste", factory: this.buildAccess },
  ];

  public render() {
    return <div className="content__item content__item--overview">
      <h2 className="home__header">Design. Develop. Deploy</h2>
      <p className="home__subtitle--main home__subtitle--first">Enhance your next MLF application.</p>
      <div className="mlf__card mlf-clear">
        {
          this.CONTENT.map((value, key) =>
            <React.Fragment key={key}>
              <h3 className="content__subheader home__subheader">{ value.header }</h3>
              <p className="home__subtitle">{ value.subheader }</p>
              { value.factory() }
            </React.Fragment>
          )
        }
      </div>
    </div>
  }

  
  private buildCards() {
    return <div className="home__container">
      {
        CARDS.map((value, key) =>
          <div className="home__container--third" key={key}>
            <SwipeCard title={value.title} main={
              <div style={{backgroundImage: `url('${value.img}')`}}/>
            }>
              <p>{ value.body }</p>
              <Button modifier="white" text="Learn More" link={value.link}/>
            </SwipeCard>
          </div>
        )
      }
    </div>
  }

  private buildPrinciples() {
    return PRINCIPLES.map((value, key) =>
      <div className="brand" key={key}>
        <div className="brand__principle">
          <h4>{value.number}</h4>
          <h5>{value.principle}</h5>
        </div>
        <div className="brand__description">
          <p>{value.body}</p>
        </div>
      </div>
    )
  }

  private buildAccess() {
    return EXAMPLES.map((value, key) =>
      <div className="home__container--gray hover__card" key={key}>
        <div className="home__container--half">
          <div className="home__example">
            { value.component }
          </div>
        </div>
        <div className="home__container--half">
          <Code>{ value.code }</Code>
        </div>
      </div>
    )
  }
}