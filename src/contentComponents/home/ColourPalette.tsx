import * as React from 'react';
import * as COLORS from '../../public/Colours';

const COLOR_TYPES = ["Pantone", "CMYK", "RGB", "HEX"];
const MAIN_COLOR = [
  { title: "Red", back: "red", color: ["1788 C", "0 100 93 0", "255 49 45", "#E8312D"] },
  { title: "Natural White", text: "red", color: ["9184 C", "0 3 10 0", "242 232 219", "#F2E9DB"] },
  { title: "Blue", back: "blue", color: ["2143 C", "100 68 0 0", "0 74 181", "#004AB5"] }
];
const SECOND_COLOR = [
  { title: ["Deep", "purple"], back: "purple", color: ["2766 C", "100 100 6 60", "26 18 45", "#1A122D"] },
  { title: ["Sky", "blue"], back: "sky-blue", color: ["2915 C", "55 4 0 0", "101 189 224", "#65BDE0"] },
  { title: ["Mustard", "yellow"], back: "yellow", color: ["142 C", "0 27 90 0", "225 168 13", "#FFA80D"] }
];

export default class Content extends React.Component {
  public render() {
    return (
    <div className="content__item content__item--colourpalette">
      <div className="mlf__card mlf-clear">
        <h2 className="content__header--first">Primary Colors</h2>
        <p>Our primary colour palette is led by Maple Leaf Red and Maple Leaf Natural White. Maple Leaf Blue should be used sparingly and alongside the red and white.</p>
        <div className="flex flex--height-1">
          <div className="flex column-half">
            { this.makeSection(MAIN_COLOR[0]) }
          </div>
          <div className="flex column-half">
            { MAIN_COLOR.slice(1).map((value, key) => this.makeSection(value, key)) }
          </div>
        </div>
      </div>

      {/* Secondary palette */}
      <div className="mlf__card mlf-clear">
        <h2 className="content__header">Secondary Colors</h2>
        <p>Our secondary palette is used to bring brightness and a sense of play into communications. However, secondary palette colours should not be used on their own. They should always live alongisde the primary palette.</p>
        <div className="flex flex--height-1">
          {
            SECOND_COLOR.map((value, key) =>
              <div key={key} className="flex column-one-third">
                { this.makeSection(value) }
              </div>
            )
          }
        </div>
      </div>

      <div className="mlf__card mlf-clear">
        <h2 className="content__header">Color Usage</h2>
        <div className="flex image--wrapper">
          {
            Object.keys(COLORS).map((value, key) => 
              <img className="palette__image" key={key} src={COLORS[value]} alt="example usage of colour palette"/>
            )
          }
        </div>
        <p>
          Use the primary colours for most brand communications. The secondary colour palette can be used in instances where the primary colours are also present.
          Do not use any of the secondary palette colours on their own. They should always live alongisde the primary palette.
        </p>
      </div>
    </div>
    );
  }

  private makeSection(
    item: { title: string | string[], back?: string, text?: string, color: string[]},
    key?: string | number
  ) {
    const { title, back, text, color } = item;
    return (
      <div key={key} className={
        `mlf__card mlf-clear palette palette--${back? back: "white"} palette__text--${text? text: "white"}`
      }>
        {
          typeof(title) === "string" ? 
          <h4 className="palette__title">Maple Leaf<br/>{ title }</h4> :
          <h4 className="palette__title">{ title[0] }<br/>{ title[1] }</h4>
        }
        <ul className="palette__colors">
          {
            color.map((value, key1) => 
              <li className="palette__color" key={key1}>
                { COLOR_TYPES[key1] }<br/>{ value }
              </li>
            )
          }
        </ul>
      </div>
    )
  }
}