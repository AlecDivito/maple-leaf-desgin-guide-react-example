const Overview = () => import('./Overview');
const ColourPalette = () => import('./ColourPalette');
const Logo = () => import('./Logo');
const Typography = () => import('./Typography');

export default {
  Overview,
  Logo,
  ColourPalette,
  Typography
}

// Put Import in here and name it (order matters)
export const meta = {
  Overview: { Title: "Overview", keywords: ["overview"], noTitle: true },
  Logo: { Title: "Logo", keywords: ["logo"] },
  ColourPalette: { Title: "Colours", keywords: ["colors"] },
  Typography: { Title: "Typography", keywords: ["typography"] }
};