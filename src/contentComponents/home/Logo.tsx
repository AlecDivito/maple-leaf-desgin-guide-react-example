import * as React from 'react';
import * as MLF from '../../public/Logo';
import { Logo } from '../../models';
import { Tooltip } from './../../formComponents/Tooltip';
import { Placement } from 'popper.js';

const LOGO_DOTS = [
  { name: "cls-1-l", placement: "left", text: "Softer blue flag with a straighter, stronger wordmark", margin: "auto auto -5% -8%" },
  { name: "cls-1-r", placement: "right", text: "Flag direction is now pointing upwards, reflecting our optimism", margin: "49% 5% auto auto" },
  { name: "cls-2", placement: "left", text: "Natural, free-floating Canadian maple leaf with a softer red colour", margin: "12% auto auto 7%" }
];
const PARTNER_DOTS = [
  { name: "prt-t", placement: "left", text: "Use partner brand's font", margin: "5% auto auto 18%" },
  { name: "prt-l", placement: "left", text: "Maintain equal with of logos", margin: "auto auto 1% 12%" }
]
const LOGO_COLORS = [
  { name: "MLF Red", color: "#E8312D" },
  { name: "MLF Neutral White", color: "#F2E9DB", dark: true },
  { name: "MLF Blue", color: "#004AB5" }
];
const PRIME_COLORS = [
  { name: "MLF Red", color: "#E8312D" },
  { name: "White", color: "#FFFFFF", dark: true },
  { name: "MLF Blue", color: "#004AB5" }
];
const COLORS = ["#E8312D", "#004AB5", "black", "#FFA80D"];
const BACK = ["#F2E9DB", "white", "#65BDE0", "#FFA80D"];
const IMG = [MLF.BACK_1, MLF.BACK_2];

export default class Content extends React.Component {

  // Content of the page
  private MAIN: IMain[] = [
    {
      title: "Overview",
      content: [{
        body: [{
          title: "Who We Are",
          content: <p>The Maple Leaf Foods logo embodies our brand design principles. Free-flowing, warm, and optimistic, it nods to our past while looking to the future.<br/><strong>To learn more, click/hover on the pulsing dots.</strong></p>
        }, {
          title: "Colors",
          content: this.overColors(LOGO_COLORS)
        }],
        img: {
          content: Logo("logo__image"),
          className: "overview",
          tooltips: LOGO_DOTS
        },
        cols: 2
      }]
    }, {
      title: "General",
      content: [{
        body: [{
          title: "Sizing & Padding",
          content: <p>The logo clear space is equal to the dimensions of the M from the wordmark. The Maple Leaf logo should not be used at sizes smaller than <strong>60px</strong></p>
        }],
        img: {
          content: <object className="logo__image" type="image/svg+xml" data={MLF.FRAME}>Maple Leaf Foods Logo</object>
        },
        cols: 2
      }]
    }, {
      title: "Variations",
      content: [
        {
          body: [{
            title: "One Color",
            content: <p>If the full color logo cannot be used, use the one colour logo. This logo should only be used in special cases where colour is limited or cannot be properly reproduced.</p>
          }],
          img: COLORS.map((color, key) => 
            ({ image: Logo("logo__img--item", "l-" + key, color) })
          ),
          cols: 1
        }, {
          body: [
            {
              title: "With Sub Brand",
              content: <p>Maintain the integrity of the logo when it's used as a masterbrand within a sub-brand.</p>
            },
            {
              title: "Colors",
              content: this.overColors(PRIME_COLORS)
            }
          ],
          img: {
            content: <img className="logo__image--prime" src={MLF.PRIME} alt="Maple Leaf Foods Prime Logo"/>,
            className: "prime"
          },
          cols: 2
        }, {
          body: [{
            title: "With Partnership",
            content: <p>Maintain the integrity of the logo when used within a partnership lock-up.</p>
          }],
          img: {
            content: <object className="logo__image" type="image/svg+xml" data={MLF.PARTNER}>Maple Leaf Foods Logo</object>,
            className: "overview",
            tooltips: PARTNER_DOTS
          },
          cols: 2
        }
      ]
    }, {
      title: "Backgrounds",
      content: [
        {
          body: [{
            title: "On Color",
            content: <p>The full colour logo should be used as much as possible. When using the logo on background colours, stick to the palette outlined within this document. However, if you cannot control the background colour, ensure all elements of the logo maintain visibility.</p>
          }],
          img: BACK.map(color => 
            ({ image: Logo("logo__img--item"), style: {backgroundColor: color} })
          ),
          cols: 1
        },
        {
          body: [{
            title: "On Image",
            content: <p>When placing the logo on photography, make sure that the logo elements are clearly visible and not covering important elements of the photograph.</p>
          }],
          img: IMG.map(img =>
            ({
              image: Logo("logo__image--top-left"),
              style: { backgroundImage: `url('${img}')` },
              className: "background"
            })
          ),
          cols: 1
        }
      ]
    }
  ];

  public render() {
    return <div className="content__item content__item--logo">
      {
        this.MAIN.map(({ title, content }: IMain, key) => 
          <div key={key} className="mlf__card mlf-clear logo__card">
            { title? <h2 className="content__header--first">{ title }</h2>: null }
            {
              content.map(({body, img, cols}: IMainContent, key1) =>
                this.buildContent(body, img, cols, key1 === 0)
              )
            }
          </div>
        )
      }
    </div>
  }

  // Main section builder
  private buildContent(body: IContentBody[], img: TContentImg, col: number, first: boolean) {
    const H = "content__subheader";
    return <div className={`logo__section logo__section--${col}${first? " logo__section--first": ""}`}>
      <div className="logo__col logo__col--text">
        {
          body.map(({title, content}, key) =>
            <>
              <h3 className={`${H}${key === 0? ` ${H}--first`: ""}`}>{ title }</h3>
              { content }
            </>
          )
        }
      </div>
      <div className="logo__col logo__col--img">
        {
          Array.isArray(img)? 
          this.listImg(img):
          this.overImg(img.content, img.className, img.tooltips)
        }
      </div>
    </div>
  }

  private overColors(colors: IColors[]) {
    return <div className="logo__colors">
      {
        colors.map(({name, color, dark}, key) =>
          <div key={key} className={`logo__color${dark? " logo__color--dark": ""}`} style={{backgroundColor: color}}>
            <h4 className="logo__color-header">{ name }</h4>
            <p className="logo__color-text">{ color }</p>
          </div>
        )
      }
    </div>
  }

  private listImg(images: IImage[]) {
    const CLASS = "logo__item";
    return <div className="logo__list">
      {
        images.map(({ image, style, className }, key) => 
          <div
            className={`${CLASS}${className? ` ${CLASS}--${className}`: ""}`}
            style={style} key={key}
          >
            { image }
          </div>
        )
      }
    </div>;
  }

  private overImg(image: any, className?: string, tooltips?: any[]) {
    const CLASS = "logo__container--";
    return (
      <div className={`${CLASS}img${className? ` ${CLASS}${className}`: ""}`}>
        { image }
        { 
          tooltips?
          tooltips.map(({ name, placement, text, margin }, key) => 
            <>
              <div key={key} id={name} className="logo__dot" style={{margin}}/>
              <Tooltip key={key} placement={placement as Placement} target={name}>
                <p className="logo__tooltip">{ text }</p>
              </Tooltip>
            </>
          ): null
        }
      </div>
    )
  }
}

interface IMain { title: string; content: IMainContent[]; }
interface IMainContent { body: IContentBody[]; img: TContentImg; cols: number; }
interface IContentBody { title: string; content: JSX.Element; }
interface IContentImg { content: JSX.Element, className?: string, tooltips?: IDots[]; }
interface IDots { name: string; placement: string; text: string; margin: string }
interface IColors { name: string, color: string, dark?: boolean }
interface IImage { image: JSX.Element, style?: React.CSSProperties, className?: string }
type TContentImg = IContentImg | IImage[];