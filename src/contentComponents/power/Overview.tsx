import * as React from 'react';

export default class Content extends React.Component {
  public render() {
    return <div className="content__item content__item--overview">
      <div className="mlf__card mlf-clear">
        <h2 className="content__header--first">Introduction</h2>
        <h3 className="content__subheader">What is powerapps?</h3>
        <p>PowerApps is rapid front end, mobile application development with vast and expanding system integration</p>
        <ul className="content__list">
          <li>PowerApps has hundreds of system connections with thousands of individual connection points</li>
          <li>The platform empowers users to rapidly build and iterate applications</li>
          <li>PowerApps will be the front end, and Flow (or Dynamics for rare complex cases) will handle business logic and app behavior </li>
        </ul>
      </div>
      <div className="mlf__card mlf-clear">
        <h2 className="content__header--first">Usage</h2>
        <h3 className="content__subheader">Examples</h3>
        <p>Powerapps is applicable in so many different ways and offers a sense of capability. Some of the applications in Maple Leaf Foods include,</p>
        <ul className="content__list">
          <li><strong>Automated software requests:</strong> Self-service app, automatically gathering approvals and updating security groups</li>
          <li><strong>FSQA:</strong> Creating forms and workflows for inspections and approvals (starting within pork)</li>
          <li><strong>Plant HR:</strong> Simplified kiosk apps for hourly interactions following union requirements (job postings, info update)</li>
          <li><strong>Service Desk forms:</strong> Front ending Service Manager (or ServiceNow) for user interaction</li>
        </ul>
        <h3 className="content__subheader">Benefits</h3>
        <p>Wherever possible and reasonable, application builds should be approached with the mindset of "PowerApps First"</p>
        <ul className="content__list">
          <li>Saves significant development time, cost, complexity and maintenance effort</li>
          <li>Simplified architecture allows for rapid iteration on changes in requirements over time</li>
          <li>Given simplicity, multiple IS resources can ramp up knowledge and contribute to backlog of apps</li>
        </ul>
        <p>Approach maximizes our investment with Microsoft systems, taking advantage of integration across O365 and 3rd party applications/platforms</p>
      </div>
    </div>
  }
}