import { meta as home } from './home';
import { meta as power } from './power';
import { meta as web } from './web';

export const meta = {
  home,
  power,
  web
}