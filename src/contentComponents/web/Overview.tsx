import * as React from 'react';

export default class Content extends React.Component {
  public render() {
    return <div className="content__item content__item--overview">
      <div className="mlf__card mlf-clear">
        <h2 className="content__header--first">Welcome</h2>
        <p>Select a page on the side to learn more about what MLF Design offers to enhance the UI experience of your next application.</p>
      </div>
    </div>
  }
}