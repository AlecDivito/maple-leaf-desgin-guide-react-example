import * as React from 'react';
import { Tooltip, Switch } from '../../formComponents'; 
import Code from '../../components/code/Code';

export default class Content extends React.Component {
  public render() {
    return <div className="content__item content__item--switch">
      <div className="mlf__card mlf-clear">
        <h2 className="content__header--first">Introduction</h2>
        <p>A switch is a way for the user to select between two states, and has similar function to a checkbox. 
          The main difference is that switches are used for instantaneous actions that take effect immediately upon toggle. 
          Switches should not be used with buttons, while checkboxes are. Switches are most effective when turning a setting on or off. 
          They should <strong>not</strong> be used to: </p>
        <ul className="content__list">
          <li>Choose between a long list of items</li>
          <li>Replace radio buttons</li>
          <li>Represent a response longer than 1 or 2 words</li>
        </ul>
      </div>
      <div className="mlf__card mlf-clear">
        <h2 className="content__header--first">Example</h2>
        <p>Below are some of these switches which works at any size using our requirements (Explained later)</p>
        <div className="content__container">
          <div className="content__container--items">
            { this.example() }
          </div>
        </div>
      </div>
      <div className="mlf__card mlf-clear">
        <h2 className="content__header--first">Usage</h2>
        <p>Being a component <strong>exclusive and custom-made</strong> under the MLF Designhub. There are some requirements to ensure it works properly in applications<sup><a id="switch-tooltip-1">1</a></sup>.
          <Tooltip placement="top" target="switch-tooltip-1">
            This is to avoid additional dependicies such as Javascript
          </Tooltip>
        . The requirements are:
        </p>
        <ul className="content__list">
          <li>The width must be <strong>1.8 times</strong> of the height. This can be done using <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/Using_CSS_variables" target="blank">CSS variables</a>, <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/calc" target="blank">CSS calc</a>, or just hardcoding it.</li>
          <li>The sizing should be defined on the <strong>"input-group"</strong> element.</li>
        </ul>
        <Code>
          <div className="input-group" style={{height: "20px", width: "calc(40px * 1.8)"}}>
            <input className="mlf__switch__input" type="checkbox"/>
            <div className="mlf__switch">
              <div className="mlf__switch__dot"/>
            </div>
          </div>
        </Code>
        <p>The toggle also comes with an disabled state, which can be enabled by disabling the checkbox input within the component</p>
      </div>
      <div className="mlf__card mlf-clear">
        <h2 className="content__header--first">Styles</h2>
        <p>By adding a new class modifier ("mlf__switch--" + "modifierName"), we can have different variations of the default MLF switch.</p>
        <div className="content__indent">

        <h3 className="content__subheader">Colour Variations</h3>
          <p>Add an additional "mlf__switch--" + "blue", "purple", "sky", or "mustard" class to change the colour of the switch.</p>
          <div className="content__container">
            <div className="content__container--items">
              <div className="switch__example__item">
                <Switch size={30} modifier="--blue"/>
              </div>
              <div className="switch__example__item">
              <Switch size={30} modifier="--purple"/>
              </div>
              <div className="switch__example__item">
              <Switch size={30} modifier="--sky"/>
              </div>
              <div className="switch__example__item">
              <Switch size={30} modifier="--mustard"/>
              </div>
            </div>
          </div>
          <Code>
          <div className="input-group" style={{height: "30px", width: "calc(30px * 1.8)"}}>
            <input className="mlf__switch__input" type="checkbox"/>
              <div className="mlf__switch mlf__switch--blue">
                <div className="mlf__switch__dot"/>
              </div>
            </div>
          </Code>
          
          <h3 className="content__subheader">With Symbols</h3>
          <p>Wrap your content within the addition tags as shown below to integrate symbols with your switch. For responsive sizing, we recommend using the defined height from before times 2.6 in percent.</p>
          <div className="content__container">
            <div className="content__container--items">
              <div className="switch__example__item">
                <Switch size={15} items={["I", "O"]}/>
              </div>
              <div className="switch__example__item">
                <Switch size={30} items={["On", "Off"]}/>
              </div>
              <div className="switch__example__item">
                <Switch size={45} modifier="--inverted" items={[<p key="a">Yes</p>, <p key="b">N</p>]}/>
              </div>
            </div>
          </div>
          <Code>
            <div className="input-group" style={{height: "20px", width: "calc(40px * 1.8)"}}>
              <input className="mlf__switch__input" type="checkbox"/>
              <div className="mlf__switch">
                <div className="mlf__switch__dot"/>
                <div className="mlf__switch__icon" style={{fontSize: "calc(28 * 2.6%)"}}>On</div>
                <div className="mlf__switch__icon" style={{fontSize: "calc(28 * 2.6%)"}}>Off</div>
              </div>
            </div>
          </Code>

          <h3 className="content__subheader">Inverted Style</h3>
          <p style={{ paddingBottom: "5px" }}>Adding <strong>"--inverted"</strong> allows the status color to appear on the dot instead of the background.</p>
          <div className="content__container">
            <div className="content__container--items">
              { this.example("--inverted") }
            </div>
          </div>
          <Code>
            <div className="input-group" style={{height: "20px", width: "calc(40px * 1.8)"}}>
              <input className="mlf__switch__input" type="checkbox"/>
              <div className="mlf__switch mlf__switch--inverted">
                <div className="mlf__switch__dot"/>
              </div>
            </div>
          </Code>

          <h3 className="content__subheader">Boolean Style</h3>
          <p style={{ paddingBottom: "5px" }}>Add <strong>"--boolean"</strong> to have a red and green background version of the default switch.</p>
          <div className="content__container">
            <div className="content__container--items">
              { this.example("--boolean") }
            </div>
          </div>
          <Code>
            <div className="input-group" style={{height: "20px", width: "calc(40px * 1.8)"}}>
              <input className="mlf__switch__input" type="checkbox"/>
              <div className="mlf__switch mlf__switch--boolean">
                <div className="mlf__switch__dot"/>
              </div>
            </div>
          </Code>

          <h3 className="content__subheader">Block Style</h3>
          <p style={{ paddingBottom: "5px" }}>Add <strong>"--block"</strong> to use a block shape rather than the default pill shape.</p>
          <div className="content__container">
            <div className="content__container--items">
              { this.example("--block") }
            </div>
          </div>
          <Code>
            <div className="input-group" style={{height: "20px", width: "calc(40px * 1.8)"}}>
              <input className="mlf__switch__input" type="checkbox"/>
              <div className="mlf__switch mlf__switch--block">
                <div className="mlf__switch__dot"/>
              </div>
            </div>
          </Code>
        </div>
      </div>
    </div>;
  }

  private example(modifier?: string) {
    const example = [
      15, 25, 35
    ]

    return example.map((value, key) => (
      <div className="switch__example__item" key={key}>
        <Switch size={value} modifier={modifier}/>
        <h3 className="switch__example__tag">{value}px</h3>
      </div>
    ));
  }
};