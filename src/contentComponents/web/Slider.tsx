import * as React from 'react';
import { Slider } from '../../formComponents';
import Code from '../../components/code/Code';

export default class Content extends React.Component {
  public render() {
    return <div className="content__item content__item--slider">
      <div className="mlf__card mlf-clear">
        <h2 className="content__header--first">Introduction</h2>
        <p>The slider or range input field is an element that allows the user to slide between numeric values. Sliders should be used only with relative quantities and not specific numeric values since users have difficulty selecting a precise value. Thus, numeric input fields, dropdowns or buttons should be used instead. Sliders should be used for: </p>
        <ul>
          <li>Audio volume</li>
          <li>Screen brightness</li>
          <li>Relative Ranges</li>
        </ul>
      </div>
      <div className="mlf__card mlf-clear">
        <h2 className="content__header--first">Examples</h2>
          <div className="content__indent">
          <h3 className="content__subheader">Default</h3>
          <div className="content__container">
            <div className="content__container--items--full">
              <Slider/>
            </div>
            <div className="content__container--caption">
              <p>The default slider ranges from 0-100 and snaps at every integer.</p>
            </div>
          </div>
          <Code>
            <input type="range" className="custom-range mlf__slider"/>
          </Code>

          <h3 className="content__subheader">Using Max and Min</h3>
          <div className="content__container">
            <div className="content__container--items--full">
              <Slider max={5}/>
            </div>
            <div className="content__container--caption">
              <p>The above slider ranges from 0-5 and has the default step of 1, thus snapping 6 times.</p>
            </div>
          </div>
          <Code>
          <input type="range" className="custom-range mlf__slider" max={5}/>
          </Code>


          <div className="content__container">
            <div className="content__container--items--full">
            <Slider min={-50} max={50}/>
            </div>
            <div className="content__container--caption">
              <p>The above slider ranges from -50 to 50 and has the default step of 1.</p>
            </div>
          </div>
          <Code>
            <input type="range" className="custom-range mlf__slider" min={-50} max={50}/>
          </Code>


          <h3 className="content__subheader">Using Step</h3>
          <div className="content__container">
            <div className="content__container--items--full">
              <Slider step={10}/>
            </div>
            <div className="content__container--caption">
              <p>The default slider ranging from 0-100 but with a step of 10, thus snapping 11 times.</p>
            </div>
          </div>
          <Code>
            <input type="range" className="custom-range mlf__slider" step={10}/>
          </Code>
        </div>
      </div>
      <div className="mlf__card mlf-clear">
        <h2 className="content__header--first">Styles</h2>
        <p>By adding modifiers ("--modifiername") to the end of the "mlf__slider" class, different slider variations may be created:</p>
        <div className="content__indent">
          <h3 className="content__subheader">Colour Variations</h3>
          <p>Adding one of the following modifiers: "red","blue","purple","sky", or "mustard" changes the colour of the slider thumb.</p>
          <div className="content__container">
            <div className="content__container--items--full">
              <Slider modifier="red"/>
            </div>
            <div className="content__container--items--full">
              <Slider modifier="blue"/>
            </div>
            <div className="content__container--items--full">
              <Slider modifier="purple"/>
            </div>
            <div className="content__container--items--full">
              <Slider modifier="sky"/>
            </div>
            <div className="content__container--items--full">
              <Slider modifier="mustard"/>
            </div>
            <div className="content__container--caption">
              <p>The default slider different slider thumb colours.</p>
            </div>
          </div>
          <Code>
            <input type="range" className="custom-range mlf__slider mlf__slider--blue"/>
          </Code>

          <h3 className="content__subheader">Inverted and Colour Variations</h3>
          <p>Adding one of the following modifiers: "inverted--" + "red","blue","purple","sky", or "mustard" changes the colour of the slider track.</p>
          <div className="content__container">
            <div className="content__container--items--full">
              <Slider modifier="inverted--red"/>
            </div>
            <div className="content__container--items--full">
              <Slider modifier="inverted--blue"/>
            </div>
            <div className="content__container--items--full">
              <Slider modifier="inverted--purple"/>
            </div>
            <div className="content__container--items--full">
              <Slider modifier="inverted--sky"/>
            </div>
            <div className="content__container--items--full">
              <Slider modifier="inverted--mustard"/>
            </div>
            <div className="content__container--caption">
              <p>The default slider with different slider track colours.</p>
            </div>
          </div>
          <Code>
            <input type="range" className="custom-range mlf__slider mlf__slider--inverted--blue"/>
          </Code>

          </div>
        </div>
    </div>;
  }
};