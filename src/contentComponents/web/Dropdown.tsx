import * as React from 'react';
import Code from '../../components/code/Code';
import { Dropdown } from '../../formComponents';

export default class Content extends React.Component {
  public render() {
    return <div className="content__item content__item--dropdown">
      <div className="mlf__card mlf-clear">
        <h2 className="content__header--first">Introduction</h2>
        <p>Dropdowns are used to help consolidate related topics into a single category to do more with less. This should be used as a form of navigation as it does not keep track of selected item(s). (In which case, a select is more suitable)</p>
      </div>
      <div className="mlf__card mlf-clear">
        <h2 className="content__header--first">Usage</h2>
        <p>To use the MLF styled dropdown, just add <strong>"mlf__dropdown"</strong> to the wrapper of the component.</p>
        <div className="content__container">
          <div className="content__container--items">
            { this.example() }
          </div>
        </div>
        <Code>
          <div className="dropdown mlf__dropdown">
            <button className="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Dropdown button
            </button>
            <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
              <a className="dropdown-item" href="#">Option 1</a>
              <a className="dropdown-item" href="#">Option 2</a>
              <a className="dropdown-item" href="#">Option 3</a>
            </div>
          </div>
        </Code>
      </div>
      <div className="mlf__card mlf-clear">
        <h2 className="content__header--first">Styles</h2>
        <div className="content__indent">
          <h3 className="content__subheader">Inverted</h3>
          <p>Add <strong>"--inverted"</strong> to the end of "mlf__dropdown" to get an inverted version to use on vibrant backgrounds.</p>
          <div className="content__container--dark">
            <div className="content__container--items">
              { this.example(" mlf__dropdown--inverted") }
            </div>
          </div>
          <Code>
            <div className="dropdown mlf__dropdown mlf__dropdown--inverted">
              <button className="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Dropdown button
              </button>
              <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
                <a className="dropdown-item" href="#">Option 1</a>
                <a className="dropdown-item" href="#">Option 2</a>
                <a className="dropdown-item" href="#">Option 3</a>
              </div>
            </div>
          </Code>

          <h3 className="content__subheader">Flat</h3>
          <p>Add <strong>"--flat"</strong> to the end of "mlf__dropdown" to get a flat themed version.</p>
          <div className="content__container">
            <div className="content__container--items">
              { this.example(" mlf__dropdown--flat") }
            </div>
          </div>
          <Code>
            <div className="dropdown mlf__dropdown mlf__dropdown--flat">
              <button className="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Dropdown button
              </button>
              <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
                <a className="dropdown-item" href="#">Option 1</a>
                <a className="dropdown-item" href="#">Option 2</a>
                <a className="dropdown-item" href="#">Option 3</a>
              </div>
            </div>
          </Code>
        </div>
      </div>
    </div>
  }

  private example = (modifier = "") => (
    <Dropdown modifier={modifier} items={[
      { content: "Option 1" },
      { content: "Option 2" },
      { content: "Option 3" }
    ]}>
      Dropdown Label
    </Dropdown>
  );
}