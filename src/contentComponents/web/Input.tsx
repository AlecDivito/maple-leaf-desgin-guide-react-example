import * as React from 'react';
import { Input } from '../../formComponents';
import Code from '../../components/code/Code';

export default class Content extends React.Component {
  public render() {
    return <div className="content__item content__item--switch">
      <div className="mlf__card mlf-clear">
        <h2 className="content__header--first">Introduction</h2>
        <p>Input fields are used to receive information from the user. It is a basic control that enables the user to type an amount of text. They are generally used as a part of:</p>
        <ul className="content__list">
          <li>Forms</li>
          <li>Registration and login fields</li>
          <li>Search fields</li>
        </ul>
        <p>Generally, input fields should follow these guidelines:</p>
        <ul className="content__list">
          <li>Have a clear and simple text label</li>
          <li>Be sized according to their expected input</li>
          <li>Include a placeholder only when the directions are unclear</li>
        </ul>
      </div>
      <div className="mlf__card mlf-clear">
        <h2 className="content__header--first">Examples</h2>
        <p>Below are variations of input fields grouped with other form elements to create a more dynamic user experience.</p>
        <div className="content__indent">
        <h3 className="content__subheader">Default</h3>
        <div className="content__container">
          <div className="content__container--items">
            <Input type="text" placeholder="Default"/>
          </div>
        </div>
        <Code>
        <div className="input-group mlf__input">
          <input type="text" className="form-control" placeholder="Default"/>
        </div>
        </Code>

          <h3 className="content__subheader">Prepend and appending text</h3>
          <p>Adding text to preceed the input is useful to provide more information or hints for the user.</p>
          <div className="content__container">
            <div className="content__container--items">
              <Input type="text" prepend="Username" placeholder="Default"/>
            </div>
          </div>
          <Code>
          <div className="input-group mlf__input">
            <div className="input-group-prepend mlf__input--prepend">
              <span className="input-group-text">Username</span>
            </div>
            <input type="text" className="form-control" placeholder="Default"/>
          </div>
          </Code>

          <div className="content__container">
            <div className="content__container--items">
              <Input type="text" append="@example.com" placeholder="Default"/>
            </div>
          </div>
          <Code>
          <div className="input-group mlf__input">
            <input type="text" className="form-control" placeholder="Default"/>
            <div className="input-group-append mlf__input--append">
              <span className="input-group-text">@example.com</span>
            </div>
          </div>
          </Code>

          <h3 className="content__subheader">Radio and check buttons</h3>
          <p>Combining multiple radio or check button input fields is useful in forms when allowing users to create their own choices.</p>
          <div className="content__container">
            <div className="content__container--items">
              <Input 
                type="text" 
                prepend={<div className='input-group-text'>
                  <input type='radio' aria-label='Radio button for following text input'/>
                </div>}
                />
            </div>
          </div>
          <Code>
          <div className="input-group mlf__input">
            <div className="input-group-prepend mlf__input--prepend">
              <div className="input-group-text">
                <input type="radio" aria-label="Radio button for following text input"/>
              </div>
            </div>
            <input type="text" className="form-control" placeholder="" id=""/>
          </div>
          </Code>

          <div className="content__container">
            <div className="content__container--items">
              <Input
                type="text" 
                prepend={<div className="input-group-text">
                <input type="checkbox" aria-label="Checkbox for following text input"/>
              </div>}
              />
            </div>
          </div>
          <Code>
          <div className="input-group mlf__input">
            <div className="input-group-prepend mlf__input--prepend">
              <div className="input-group-text">
                <input type="checkbox" aria-label="Checkbox for following text input"/>
              </div>
            </div>
            <input type="text" className="form-control" placeholder="" id=""/>
          </div>
          </Code>

          <h3 className="content__subheader">Appending button</h3>
          <p>An integrated button is useful for quickly submitting information such as for searching.</p>
          <div className="content__container">
            <div className="content__container--items">
              <Input
                type="text" 
                prepend={<button className="btn btn-outline-secondary" type="button">Button</button>}
              />
            </div>
          </div>
          <Code>
          <div className="input-group mlf__input">
            <div className="input-group-prepend mlf__input--prepend">
              <button className="btn btn-outline-secondary" type="button">
                Button
              </button>
            </div>
            <input type="text" className="form-control" placeholder="" id=""/>
          </div>
          </Code>

          <h3 className="content__subheader">Multi-line Input</h3>
          <p>Multi-line input fields should be used when large amounts of information is required.</p>
          <div className="content__container">
            <div className="content__container--items">
              <Input
                prepend="Multi-line Input"
                alternativeInput={
                  <textarea className="form-control" aria-label="With textarea"/>
                }
              />
            </div>
          </div>
          <Code>
          <div className="input-group mlf__input">
            <div className="input-group-prepend mlf__input--prepend">
              <span className="input-group-text">textarea</span>
            </div>
            <textarea className="form-control" aria-label="With textarea"/>
          </div>
          </Code>
        </div>

        <h2 className="content__header--first">Styles</h2>
        <div className="content__indent">

        <h3 className="content__subheader">Prepend and Append Colour Variations</h3>
          <p>Add an additional class "mlf__input" + "--white", "--red", "--blue", "--purple", "--sky" or "--mustard" to change the colour of the prepend or append.</p>
          <div className="content__container">
            <div className="content__container--items">
              <Input type="text" prepend="Prepend" prependModifier="--white" placeholder="Placeholder"/>
              <Input type="text" prepend="Prepend" prependModifier="--red" placeholder="Placeholder"/>
              <Input type="text" prepend="Prepend" prependModifier="--blue" placeholder="Placeholder"/>
              <Input type="text" prepend="Prepend" prependModifier="--purple" placeholder="Placeholder"/>
              <Input type="text" append="Append" appendModifier="--sky" placeholder="Placeholder"/>
              <Input type="text" append="Append" appendModifier="--mustard" placeholder="Placeholder"/>
            </div>
          </div>
          <Code>
            /* White Variant */
          <div className="input-group mlf__input">
            <div className="input-group-prepend mlf__input--prepend mlf__input--white">
              <span className="input-group-text">Prepend</span>
            </div>
            <input type="text" className="form-control" placeholder="" id=""/>
          </div>
          </Code>
          
          <h3 className="content__subheader">Flat</h3>
          <p>Add an additional class "mlf__input--flat" to the outermost div to use a flat style.</p>
          <div className="content__container">
            <div className="content__container--items">
              <Input type="text" modifier="--flat" placeholder="Placeholder"/>
            </div>
          </div>
          <Code>
          <div className="input-group mlf__input--flat">
            <input type="text" className="form-control" placeholder="" id=""/>
          </div>
          </Code>

          <h3 className="content__subheader">Flat Colour Variations</h3>
          <p>Combine the colour classes in the inner div With the "mlf__input--flat" class to create flat input fields with colour variations.</p>
          <div className="content__container">
            <div className="content__container--items">
              <Input type="text" prepend="Prepend" prependModifier="--red" modifier="--flat" placeholder="Placeholder"/>
            </div>
          </div>
          <Code>
            /* Red Variant */
          <div className="input-group mlf__input--flat">
            <div className="input-group-prepend mlf__input--prepend mlf__input--red">
              <span className="input-group-text">Prepend</span>
            </div>
            <input type="text" className="form-control" placeholder="" id=""/>
          </div>
          </Code>

        </div>
      </div>
    </div>;
  }
};