import * as React from 'react';
import { Tooltip } from '../../formComponents';
import Code from '../../components/code/Code';
import { Logo } from '../../models';

export default class Content extends React.Component {
  private NUMBER = -1;

  public render() {
    return <div className="content__item content__item--tooltip">
      <div className="mlf__card mlf-clear">
        <h2 className="content__header--first">Introduction</h2>
        <p>Tooltips are used to provide further insight and/or information to content. Some examples of how tooltips can be used effectively are...</p>
        <ul className="content__list">
          <li>Showing further instructions to an input</li>
          <li>Providing a definition to technical and/or complex terms</li>
          <li>and more...</li>
        </ul>
      </div>
      <div className="mlf__card mlf-clear">
        <h2 className="content__header--first">Example</h2>
        <p style={{paddingBottom: "10px"}}>Hover over the images to see the new style of the <strong>top, right, bottom, left</strong> tooltips</p>
        <div className="content__container">
          <div className="content__container--items">
            {this.example()}
          </div>
          <div className="content__container--caption"><p>Default - Example</p></div>
        </div>
      </div>
      <div className="mlf__card mlf-clear">
        <h2 className="content__header--first">Usage</h2>
        <p>MLF's custom tooltips requires a <strong>modest</strong> amount of setup to get working with Bootstrap by replacing the tooltip template with the code below as a options when initializing.</p>
        <Code>
          /* Add "mlf__tooltip" to the class of default template */
          <div className="tooltip mlf__tooltip" role="tooltip">
            <div className="tooltip-arrow"/>
            <div className="tooltip-inner"/>
          </div>
        </Code>
      </div>
      <div className="mlf__card mlf-clear">
        <h2 className="content__header--first">Styles</h2>
        <p>By adding modifiers ("--xxxxx") to the end of the "mlf__tooltip" class, we can have different variations of the default MLF tooltips. We would explore some of these below:</p>
        <div className="content__indent">
          <h3 className="content__subheader">Dark Theme</h3>
          <p style={{ paddingBottom: "5px" }}>Add <strong>"--dark"</strong> to have an dark themed version of the default tooltip</p>
          <div className="content__container">
            <div className="content__container--items">
              {this.example("mlf__tooltip--dark")}
            </div>
          </div>
          <Code>
            /* Add "mlf__tooltip--dark" to the class of default template */
            <div className="tooltip mlf__tooltip mlf__tooltip--dark" role="tooltip">
              <div className="tooltip-arrow" />
              <div className="tooltip-inner" />
            </div>
          </Code>

          <h3 className="content__subheader">Pill-Shaped Theme</h3>
          <p style={{ paddingBottom: "5px" }}>Add <strong>"--pill"</strong> to have a pill shaped version of the default tooltip. By default, this would be using the original theme, however it can be further modified to be dark themed by adding it after the dark theme modifier ("--dark--pill")</p>

          <div className="content__container">
            <div className="content__container--items">
              {this.example("mlf__tooltip--pill")}
            </div>
          </div>
          <Code>
            /* Add "mlf__tooltip--pill" to the class of default template */
            <div className="tooltip mlf__tooltip mlf__tooltip--pill" role="tooltip">
              <div className="tooltip-arrow" />
              <div className="tooltip-inner" />
            </div>
          </Code>
        </div>
      </div>
    </div>
  }

  private example(modifier?: string) {
    const example: ["top", "right", "bottom", "left"] = [
      "top", "right", "bottom", "left"
    ];

    this.NUMBER++;
    return example.map((value, key) => {
      return <div className="tooltip__image" key={key}>
        { Logo("tooltip__logo", `tooltip-${key}-${this.NUMBER}`) }
        <Tooltip placement={value} target={"tooltip-" + key + "-" + this.NUMBER} className={ modifier }>
          <span>This tooltip is placement is <strong>{value}</strong></span>
        </Tooltip>
      </div>
    })
  }
}