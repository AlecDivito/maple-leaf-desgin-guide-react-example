const Input = () => import('./Input');
const Overview = () => import('./Overview');
const Tooltip = () => import('./Tooltip');
const Button = () => import('./Button');
const Switch = () => import('./Switch');
const Dropdown = () => import('./Dropdown');
const Radio = () => import('./Radio');
const Checkbox = () => import('./Checkbox');
const Slider = () => import('./Slider');

export default {
  Input,
  Overview,
  Tooltip,
  Button,
  Switch,
  Dropdown,
  Radio,
  Checkbox,
  Slider
};

// Put Import in here and name it (order matters)
export const meta = {
  Overview: { Title: "Overview", keywords: ["overview"] },
  Input: { Title: "Input", keywords: ["input"] },
  Tooltip: { Title: "Tooltips", keywords: ["tooltips"] },
  Button: { Title: "Button", keywords: ["button"] },
  Switch: { Title: "Switch", keywords: ["switch"] },
  Dropdown: { Title: "Dropdown", keywords: ["dropdown"] },
  Radio: { Title: "Radio", keywords: ["radio"] },
  Checkbox: { Title: "Checkbox", keywords: ["checkbox"] },
  Slider: { Title: "Slider", keywords: ["slider"] }
};