import * as React from 'react';
import { Checkbox } from '../../formComponents';
import Code from '../../components/code/Code';

export default class Content extends React.Component {
  public render() {
    return <div className="content__item content__item--checkbox">
      <div className="mlf__card mlf-clear">
        <h2 className="content__header--first">Introduction</h2>
        <p>Checkboxes permit the user to make a choice between two states. They are used in forms when the user may select any number and of choices, including one, zero or several.</p>
      </div>
      <div className="mlf__card mlf-clear">
        <h2 className="content__header--first">Example</h2>
        <p>Any combination of choices may be selected from the options below.</p>
        <div className="content__container">
        <div className="content__container--list">
            <Checkbox text="Beef" id="example1"/>
            <Checkbox text="Chicken" id="example2"/>
            <Checkbox text="Pork" id="example3"/>
            <Checkbox text="Vegetarian" id="example4"/>
          </div>
        </div>
      </div>
      <div className="mlf__card mlf-clear">
        <h2 className="content__header--first">Usage</h2>
        <p>Checkboxes are creating using Bootstrap's custom checkbox style by hiding the default browser checkbox and creating one manually. Add a class of "mlf__checkbox" to the wrapping div, "mlf__checkbox__input" to the input element and "mlf__checkbox__label" to the label.</p>
        <Code>
          <div className="mlf__checkbox custom-checkbox">
            <input type="checkbox" className="custom-control-input mlf__checkbox__input" id="idName"/>
            <label className="custom-control-label mlf__checkbox__label" htmlFor="idName">
              Text
            </label>
          </div>
        </Code>
        <h2 className="content__header--first">Styles</h2>
        <p>By adding a new modifier class ("mlf__checkbox--modifiername") to the outermost div, various checkbox styles may be created:</p>
        <div className="content__indent">

        <h3 className="content__subheader">Default</h3>
        <p>The default checkbox style should be used in most cases. To use a disabled checkbox, simply add "disabled" as an attribute to the input element.</p>
        <div className="content__container">
            <div className="content__container--items">
              <Checkbox text="Default Checkbox" id="default1"/>
            </div>
            <div className="content__container--items">
              <Checkbox text="Disabled Default Checkbox" id="default2" disabled="true"/>
            </div>
          </div>
        <Code>
          <div className="custom-checkbox mlf__checkbox">
            <input type="checkbox" className="custom-control-input mlf__checkbox__input" id="idName"/>
            <label className="custom-control-label mlf__checkbox__label" htmlFor="idName">
              Default Checkbox
            </label>
          </div>
        </Code>
        
        <h3 className="content__subheader">Round</h3>
        <p>Add the class "mlf__checkbox--round" to use a round checkbox rather than a square one.</p>
        <div className="content__container">
            <div className="content__container--items">
              <Checkbox modifier="round" text="Round Checkbox" id="round1"/>
            </div>
            <div className="content__container--items">
              <Checkbox modifier="round" text="Disabled Round Checkbox" id="round2" disabled="true"/>
            </div>
          </div>
        <Code>
          <div className="custom-checkbox mlf__checkbox mlf__checkbox--round">
            <input type="checkbox" className="custom-control-input mlf__checkbox__input" id="idName"/>
            <label className="custom-control-label mlf__checkbox__label" htmlFor="idName">
              Round Checkbox
            </label>
          </div>
        </Code>

        <h3 className="content__subheader">Flat</h3>
        <p>Add the class "mlf__checkbox--flat" to use a flat checkbox style. The flat checkbox style should be used with other flat elements.</p>
        <div className="content__container">
            <div className="content__container--items">
              <Checkbox modifier="flat" text="Flat Checkbox" id="flat1"/>
            </div>
            <div className="content__container--items">
              <Checkbox modifier="flat" text="Disabled Flat Checkbox" id="flat2" disabled="true"/>
            </div>
          </div>
          <Code>
          <div className="custom-checkbox mlf__checkbox mlf__checkbox--flat">
            <input type="checkbox" className="custom-control-input mlf__checkbox__input" id="idName"/>
            <label className="custom-control-label mlf__checkbox__label" htmlFor="idName">
              Flat Checkbox
            </label>
          </div>
        </Code>

        <h3 className="content__subheader">Round Flat</h3>
        <p>Add both the classes "mlf__checkbox--round" and "mlf__checkbox--flat" to combine the two styles</p>
        <div className="content__container">
            <div className="content__container--items">
              <Checkbox modifier="flat mlf__checkbox--round" text="Round Flat Checkbox" id="roundFlat1"/>
            </div>
            <div className="content__container--items">
              <Checkbox modifier="flat mlf__checkbox--round" text="Round Flat Checkbox" id="roundFlat2" disabled="true"/>
            </div>
          </div>
          <Code>
          <div className="custom-checkbox mlf__checkbox mlf__checkbox--flat mlf__checkbox--round">
            <input type="checkbox" className="custom-control-input mlf__checkbox__input" id="idName"/>
            <label className="custom-control-label mlf__checkbox__label" htmlFor="idName">
              Round Flat Checkbox
            </label>
          </div>
        </Code>

        </div>
      </div>
    </div>;
  }
};