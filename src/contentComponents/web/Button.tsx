import * as React from 'react';
import { Button } from '../../formComponents';
import Code from '../../components/code/Code';

export default class Content extends React.Component {
  public render() {
    return <div className="content__item content__item--button">
      <div className="mlf__card mlf-clear">
        <h2 className="content__header--first">Introduction</h2>
        <p>Buttons play a major part of any user interface, and are used to trigger events from anything such as navigating to a new page, loading more content, or submitting a form.</p>
      </div>
      <div className="mlf__card mlf-clear">
        <h2 className="content__header--first">Styles</h2>
        <p>By adding a new modifier class ("mlf__button--modifiername") to the button, different button variations may be created:</p>
        <div className="content__indent">
          <h3 className="content__subheader">Default</h3>
          <p>The default button style should be used in most cases.</p>
          <div className="content__container">
            <div className="content__container--items">
              <Button text="Default Button"/>
            </div>
          </div>
          <Code>
            <button className="btn mlf__button" role="button" type="submit">Default Button</button>
          </Code>

          <h3 className="content__subheader">Color Variants</h3>
          <p>Adding one of the following modifiers: "blue","white","purple","sky", or "mustard", changes the colour of the default button style.</p>
          <div className="content__container">
            <div className="content__container--items">
              <Button modifier="blue" text="Default Button"/>
              <Button modifier="purple" text="Default Button"/>
              <Button modifier="sky" text="Default Button"/>
              <Button modifier="mustard" text="Default Button"/>
            </div>
          </div>
          <Code>
            <button className="btn mlf__button mlf__button--blue" role="button" type="submit">Default Button</button>
          </Code>
          <div className="content__container--dark">
            <div className="content__container--items">
              <Button modifier="white" text="Default Button"/>
            </div>
          </div>
          <Code>
            <button className="btn mlf__button mlf__button--white" role="button" type="submit"/>
          </Code>

          <h3 className="content__subheader">Square</h3>
          <p>The square button style including the left or right-facing chevron icon should be used when directing users to a previous or upcoming section.</p>
          <div className="content__container">
            <div className="content__container--items">
              <Button modifier="square" text="Square Button" icon="chevron_left"/>
            </div>
            <div className="content__container--items">
              <Button modifier="square" text="Square Button" icon="chevron_right"/>
            </div>
          </div>
          <Code>
            <button className="btn mlf__button mlf__button--square" role="button" type="submit"/>
          </Code>

          <h3 className="content__subheader">Flat</h3>
          <p>The flat button should be used in addition to other flat elements.</p>
          <div className="content__container">
            <div className="content__container--items">
              <Button modifier="flat" text="Flat Button"/>
            </div>
          </div>
          <Code>
            <button className="btn mlf__buton mlf__button--flat" role="button"  type="submit"/>
          </Code>
        </div>
      </div>
    </div>;
  }
};