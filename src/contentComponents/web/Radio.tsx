import * as React from 'react';
import { Radio } from '../../formComponents';
import Code from '../../components/code/Code';

interface IState {
  normal: number,
  square: number,
  flat: number,
  flatSquare: number
}
export default class Content extends React.Component<any, IState> {
  constructor(props: any) {
    super(props);
    this.state = {
      normal: 1,
      square: 1,
      flat: 1,
      flatSquare: 1
    }
    this.normalRadioClick = this.normalRadioClick.bind(this);
    this.squareRadioClick = this.squareRadioClick.bind(this);
    this.flatRadioClick = this.flatRadioClick.bind(this);
    this.flatSquareRadioClick = this.flatSquareRadioClick.bind(this);
  }
  public normalRadioClick(indexSelected: number) {
    this.setState({normal: indexSelected});
  }
  public squareRadioClick(indexSelected: number) {
    this.setState({square: indexSelected});
  }
  public flatRadioClick(indexSelected: number) {
    this.setState({flat: indexSelected});
  }
  public flatSquareRadioClick(indexSelected: number) {
    this.setState({flatSquare: indexSelected});
  }
  public render() {
    return <div className="content__item content__item--radio">
      <div className="mlf__card mlf-clear">
        <h2 className="content__header--first">Introduction</h2>
        <p>A radio button is a type of form input that allows users select one option out of many in a list format.</p>
      </div>
      <div className="mlf__card mlf-clear">
        <h2 className="content__header">Styles</h2>
        <p>Radio buttons can be customized by adding an additional class "mlf__radio--modifierName" to the span element. The logic for dynamically toggling checked radio buttons is not included.</p>
        
        <h3 className="content__subheader">Default</h3>
        <p>A normal MLF themed radio group created by wrapping contents within the label class.
           The Label class can be swapped out however, the class name must remain. 
        </p>
        <div className="content__container">
          <div className="content__container--list">
            { this.example( this.state.normal, "Option", this.normalRadioClick )}
          </div>
        </div>
        <Code>
          <div>
            <label className="mlf__radio">Option 1
              <input type="radio" className="mlf__radio__button"/>
              <span className="mlf__radio__dot"/>
            </label>
            <label className="mlf__radio">Option 2
              <input type="radio" className="mlf__radio__button"/>
              <span className="mlf__radio__dot"/>
            </label>
            <label className="mlf__radio">Option 3
              <input type="radio" className="mlf__radio__button"/>
              <span className="mlf__radio__dot"/>
            </label>
          </div>
        </Code>
        <h3 className="content__subheader">Square Radio</h3>
        <p>Create a square themed radio button by adding the class "mlf__radio--square" to the radio dot.
        </p>
        <div className="content__container">
          <div className="content__container--list">
        { this.example( this.state.square, "Square", this.squareRadioClick, "--square")}
          </div>
        </div>
        <Code>
          <div>
            <label className="mlf__radio">Square 1
              <input type="radio" className="mlf__radio__button"/>
              <span className="mlf__radio__dot mlf__radio--square"/>
            </label>
            <label className="mlf__radio">Square 2
              <input type="radio" className="mlf__radio__button"/>
              <span className="mlf__radio__dot mlf__radio--square"/>
            </label>
            <label className="mlf__radio">Square 3
              <input type="radio" className="mlf__radio__button"/>
              <span className="mlf__radio__dot mlf__radio--square"/>
            </label>
          </div>
        </Code>
        <h3 className="content__subheader">Flat Radio</h3>
        <p>Create a flat themed radio button by adding the class "mlf__radio--flat" to the radio dot.
        </p>
        <div className="content__container">
          <div className="content__container--list">
            { this.example( this.state.flat, "Flat", this.flatRadioClick, "--flat")}
          </div>
        </div>
        <Code>
          <div>
            <label className="mlf__radio">Flat 1
              <input type="radio" className="mlf__radio__button"/>
              <span className="mlf__radio__dot mlf__radio--flat"/>
            </label>
            <label className="mlf__radio">Flat 2
              <input type="radio" className="mlf__radio__button"/>
              <span className="mlf__radio__dot mlf__radio--flat"/>
            </label>
            <label className="mlf__radio">Flat 3
              <input type="radio" className="mlf__radio__button"/>
              <span className="mlf__radio__dot mlf__radio--flat"/>
            </label>
          </div>
        </Code>
        <h3 className="content__subheader">Flat and Square Radio</h3>
        <p>Create a flat and square themed radio button by adding the class "--flat-square" to the radio dot.
        </p>
        <div className="content__container">
          <div className="content__container--list">
            { this.example( this.state.flatSquare, "Flat Square", this.flatSquareRadioClick, "--flat-square")}
          </div>
        </div>
        <Code>
          <div>
            <label className="mlf__radio">Flat Square 1
              <input type="radio" className="mlf__radio__button"/>
              <span className="mlf__radio__dot mlf__radio--flat-square"/>
            </label>
            <label className="mlf__radio">Flat Square 2
              <input type="radio" className="mlf__radio__button"/>
              <span className="mlf__radio__dot mlf__radio--flat-square"/>
            </label>
            <label className="mlf__radio">Flat Square 3
              <input type="radio" className="mlf__radio__button"/>
              <span className="mlf__radio__dot mlf__radio--flat-square"/>
            </label>
          </div>
        </Code>
      </div>
    </div>;
  }
  private example(stateProp:number, label: string, clickEvent: (indexSelected:number) => void, modifier?: string) {
    const example = [
      1, 2, 3 
    ]
    return example.map((value, key) => (
      <span key={key} onClick={(e) => clickEvent(value)}>
        <Radio 
          modifier={modifier} 
          active={stateProp === value} 
          label={label + " " + value}
        />
      </span>
    ));
  }
};