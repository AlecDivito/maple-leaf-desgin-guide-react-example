export enum Resolution {
  PHONE = 480,
  TABLET = 900,
  DESKTOP = 1024,
  DESKTOP_LARGE = 1800,
}