import * as React from 'react';
import { IsMobile } from '../../util/index';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { onSwipingLeft, onSwipingRight, onSwipingUp, onSwipingDown } from '../../redux/swipeActions';

interface IProps extends IDispatch {
  delta?: number;
  flickThreshold?: number;
  enabled: boolean;
  LeftAction?: (dispatch: any, getState?: () => any) => void;
  RightAction?: (dispatch: any, getState?: () => any) => void;
  UpAction?: (dispatch: any, getState?: () => any) => void;
  DownAction?: (dispatch: any, getState?: () => any) => void;
}

interface IState {
  x: number | null,
  y: number | null,
  swiping: boolean,
  start: number
}

class Swipe extends React.Component<IProps, IState> {

  public static defaultProps = {
    delta: 10,
    flickThreshold: 0.1
  };

  public componentWillMount() {
    this.setState({
      start: 0,
      x: null,
      y: null,
    })
  }

  public componentDidMount() {
    document.addEventListener("touchstart", this.handleTouchStart, {passive: true});
    document.addEventListener("touchmove", this.handleTouchMove, {passive: true});
  }

  public componentWillUnmount() {
    document.removeEventListener("touchstart", this.handleTouchStart);
    document.removeEventListener("touchmove", this.handleTouchMove);
  }

  public handleTouchStart = (event: TouchEvent) => {
    this.setState({
      start: Date.now(),
      x: event.touches[0].clientX,
      y: event.touches[0].clientY,
    })
  }

  public handleTouchMove = (event: TouchEvent) => {
    const flickThreshold = this.props.flickThreshold as number;
    const delta = this.props.delta as number;
    const { x, y, start } = this.state;
    const { LeftAction, RightAction, UpAction, DownAction, enabled } = this.props;
    if (!x || !y) {
      return;
    }

    const xUp = event.touches[0].clientX;
    const yUp = event.touches[0].clientY;

    const xDiff = x - xUp;
    const yDiff = y - yUp;

    const absX = Math.abs(xDiff);
    const absY = Math.abs(yDiff);

    const time = Date.now() - start;
    const velocity = Math.sqrt(absX * absX + absY * absY) / time;

    const isFlick = velocity > flickThreshold;
    if (IsMobile() && enabled) {
      if (Math.abs(xDiff) > Math.abs(yDiff) && isFlick) {
        if (xDiff > 0 && xDiff > delta && LeftAction) {
          this.props.onSwipingLeft(LeftAction);
        } else if (xDiff < 0 && xDiff < delta && RightAction) {
          this.props.onSwipingRight(RightAction);
        }
      } else {
        if (yDiff > 0 && yDiff > delta && UpAction) {
          this.props.onSwipingUp(UpAction);
        } else if (yDiff < 0 && yDiff < delta && DownAction) {
          this.props.onSwipingDown(DownAction);
        }
      }
    }

    this.setState({
      x: null,
      y: null
    })
  }

  public render() {
    return null;
  }
}

const mapStateToProps = (state: any) => ({
  enabled: state.swipe.enabled
});
interface IDispatch {
  onSwipingLeft: (action: (dispatch: any, getState?: () => any) => void) => void;
  onSwipingRight: (action: (dispatch: any, getState?: () => any) => void) => void;
  onSwipingUp: (action: (dispatch: any, getState?: () => any) => void) => void;
  onSwipingDown: (action: (dispatch: any, getState?: () => any) => void) => void;
}
const mapDispatchToProps = (dispatch: any) => bindActionCreators({
  onSwipingLeft,
  onSwipingRight,
  onSwipingUp,
  onSwipingDown
}, dispatch);

export default connect<any, IDispatch, any>(mapStateToProps, mapDispatchToProps)(Swipe);