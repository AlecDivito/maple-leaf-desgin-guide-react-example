import * as React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { ROUTES } from '../../';
import Search from '../search/Search';
import Mobilenav from '../mobilenav/Mobilenav';
import { toggleSidenav, nextComponent } from '../../redux/sidenavActions';
import { toggleMobilenav, setCurrentPage, toggleSearch } from '../../redux/navActions';

interface IProps extends IState, IDispatch {  }
class Nav extends React.Component<IProps> {

  private icons = [
    { icon: "vertical_split", action: this.props.toggleSidenav },
    { icon: "apps", action: this.props.toggleMobilenav }
  ]

  constructor(props: IProps) {
    super(props);
    this.toggleSearch = this.toggleSearch.bind(this);
    this.props.setCurrentPage(location.pathname, location.hash.slice(1), true);
  }

  public render() {
    const { active, scrolled, search } = this.props;
    return <div className="mlf-clear--all">
      <nav className={`nav${scrolled? "--scrolled": ""}`}>
        <ul className="nav__items">
        {
          ROUTES.map(({ name, url }, index) => {
            return <li
              className={ "nav__item" + (active === url? "--active" : "") }
              onClick={this.props.setCurrentPage.bind(this, url, "", false, 400)}
              key={ index }
            >
              <a className="nav__link">{ name }</a>
            </li>
          })
        }
        </ul>
        {
          this.icons.map(({ icon, action }, key) => 
            <i
            className={`nav__icon material-icons`}
            key={ key } onClick={ action }
            >
              { icon }
            </i>
          )
        }
        <i
        className={`nav__icon nav__icon--search${search? " nav__icon--close": ""} material-icons`}
        onClick={ this.toggleSearch }
        >
          search
        </i>
        <Search/>
      </nav>
      <Mobilenav/>
    </div>
  }

  private toggleSearch() {
    const { search, sidenav } = this.props;
    if (sidenav) {
      this.props.toggleSidenav();
    }
    this.props.toggleSearch(!search, search? 400: 0);
  }
}

interface IState {
  active: string;
  scrolled: boolean;
  search: boolean;
  sidenav: boolean
};
const mapStateToProps = (state: any) => ({
  active: state.nav.active,
  scrolled: state.nav.scrolled,
  search: state.nav.search,
  sidenav: state.sidenav.expanded
});

interface IDispatch {
  toggleMobilenav: () => { type: string };
  nextComponent: (key: string, delay: number) => void;
  setCurrentPage: (active: string, component?: string | null, stay?: boolean, delay?: number) => (dispatch: any) => void;
  toggleSidenav: () => { type: string };
  toggleSearch: (state: boolean, delay: number) => (dispatch: any) => void;
}
const mapDispatchToProps = (dispatch: any) => bindActionCreators({
  toggleMobilenav,
  nextComponent,
  setCurrentPage,
  toggleSidenav,
  toggleSearch
}, dispatch);

export default connect<IState, IDispatch>(mapStateToProps, mapDispatchToProps)(Nav);
