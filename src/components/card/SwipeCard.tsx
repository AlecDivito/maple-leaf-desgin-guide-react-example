import * as React from 'react';

interface IProps {
    title: string;
    main: any;
}

export default class SwipeCard extends React.Component<IProps> {
    public render() {
        const { main, title, children } = this.props;
        return <div className="swipe-card">
            <div className="swipe-card__main">
                { main }
            </div>
            <div className="swipe-card__slider">
                <h3>{ title }</h3>
                { children }
            </div>
        </div>;
    }
  }