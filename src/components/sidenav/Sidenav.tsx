import * as React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { IsMobile } from '../../util/index';
import { nextComponent, toggleSidenav } from '../../redux/sidenavActions';
import { setCurrentPage } from '../../redux/navActions';
import Logo from '../../public/logo.svg';

interface Iitem { Title: string; keywords: string[] }
interface IProps extends IStateProps, IDispatchProps { 
  items: { [name: string]: Iitem };
  fadeout: boolean;
}

interface IState { done: boolean }
class SidenavComponent extends React.Component<IProps, IState> {

  constructor(props: IProps) {
    super(props);
    this.state = { done: false };
    this.home = this.home.bind(this);
  }

  public componentDidMount() {
    if (this.props.expanded) {
      this.props.toggleSidenav();
    }
    setTimeout(() => this.setState({done: true}), 400);
  }

  public home() {
    this.props.setCurrentPage("/", "", false, 400);
  }

  public nextComponent(key: string, active: string) {
    if (key !== active) {
      if (IsMobile() && this.props.expanded) {
        this.props.toggleSidenav();
      }
      this.props.nextComponent(key, 400);
    }
  }

  public render() {
    const { expanded, active, items, fadeout } = this.props;
    const { done } = this.state;
    return (
      <div className={
        `mlf-clear--all ${done? `sidenav--${expanded? "expanded": "collapsed"}`: "sidenav"}${fadeout? " sidenav--fadeout": ""}`
      }>
        <div className="sidenav__brand">
          <img onClick={this.home} className="sidenav__logo" src={Logo} alt="Design Hub Logo" />
        </div>
        <div className="sidenav__wrapper--items">
          <ul className="sidenav__items">
          {
            items?
            Object.keys(items).map(key => 
              <li 
                className={`sidenav__item${active === key ? " sidenav__item--active" : ""}`}
                onClick={this.nextComponent.bind(this, key, active)}
                key={key}
              >
                { items[key].Title }
              </li>
            ):
            null
          }
          </ul>
        </div>
      </div>
    )
  }
}

interface IStateProps {
  active: string;
  expanded: boolean;
};
const mapStateToProps = (state: any) => ({
  active: state.sidenav.active,
  expanded: state.sidenav.expanded
});

interface IDispatchProps {
  nextComponent: (key: string, delay?: number) => void;
  setCurrentPage: (active: string, component?: string, stay?: boolean, delay?: number) => (dispatch: any) => void;
  toggleSidenav: () => void;
};
const mapDispatchToProps = (dispatch: any) => bindActionCreators({
  nextComponent,
  toggleSidenav,
  setCurrentPage
}, dispatch);

export default connect<IStateProps, IDispatchProps>(mapStateToProps, mapDispatchToProps)(SidenavComponent);
