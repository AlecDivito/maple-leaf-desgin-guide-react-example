import * as React from 'react';

interface IProps {
  moduleProvider: () => Promise<any>;
  uid?: string;
  props?: any;
}
interface IState {
  Component: any;
}
export default class AsyncComponent extends React.PureComponent<IProps, IState> {
  constructor(props: IProps) {
    super(props);

    this.state = {
      Component: null
    }
  }

  public componentDidMount() {
    if (this.props.moduleProvider) {
      this.props.moduleProvider().then((Component: any) => {
        this.setState({
          Component: Component.default
        });
      });
    }
  }

  public componentDidUpdate(prevProps: IProps) {
    const { uid, moduleProvider } = this.props;
    if (prevProps.uid !== uid) {
      this.setState({Component: null});
      moduleProvider().then((Component: any) => {
        this.setState({
          Component: Component.default
        });
      });
    }
  }

  public render() {
    const { Component } = this.state;
    if (Component) {
      return <Component { ...this.props.props }/>
    }

    return (
      <div className="async">
        <div className="async__loader"/>
        <h2 className="async__text">Loading</h2>
      </div>
    )
  }
};