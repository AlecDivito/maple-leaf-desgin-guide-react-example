import * as React from 'react';
import { store } from '../../index';
import { DISABLE_SWIPE, ENABLE_SWIPE } from './../../redux/swipeActions';


const TAB = "    "; // Defines a tab
const CLOSE = ["link", "input"]; // Defines self-closing elements
let timeout:NodeJS.Timer; // Timer for disabling swipe
let key = 0; // For Making unique keys for React

class Code extends React.Component {

  private ref: React.RefObject<HTMLPreElement>;
  constructor(props: {}) {
    super(props);
    this.ref = React.createRef();
  }

  public render() {
    return (
      <pre className="code__editor" ref={this.ref}>
        <ol className="mlf-clear--all code__lines">
          { this.buildChildren(this.props.children) }
        </ol>
      </pre>
    );
  }

  public componentDidMount() {
    const ref = this.ref.current;
    if (ref) {
      ref.addEventListener("touchstart", this.handleScroll, {passive: true});
      ref.addEventListener("touchmove", this.handleScroll, {passive: true});
    }
  }

  public componentWillUnmount() {
    const ref = this.ref.current;
    clearTimeout(timeout);

    if (ref) {
      ref.removeEventListener("touchstart", this.handleScroll);
      ref.removeEventListener("touchmove", this.handleScroll);
    }
  }

  // Recursive function that handles different types of children
  // children: The children of a node
  // indent: How much indent for line
  // wrap: Whether string should be an individual code__line [ <li>{ ... }</li> ]
  private buildChildren(children: React.ReactNode, indent = 0, wrap = true): React.ReactNode {
    let res: React.ReactNode;
    switch(this.getType(children)) {
      case "string":
        children = <span className="code__text">{children}</span>;
        res = (wrap? this.codeLine(children, indent): children);
        break;
      case "array":
        res = (children as React.ReactNodeArray).map(child =>
          this.buildChildren(child, indent)
        );
        break;
      case "object":
        const node = this.sortProps((children as React.ReactElement<any>).props);
        const tag = (children as any).type;

        // Self closing
        if (CLOSE.indexOf(tag) > -1) {
          res = this.codeLine(
            this.buildElement(tag, "", node.props, true),
            indent
          );
        } else

        // Single line
        if (!node.children || this.getType(node.children) === "string") {
          res = this.codeLine(
            this.buildElement(
              tag,
              this.buildChildren(node.children, indent, false),
              node.props
            ),
            indent
          );
        } else

        // Multi line
        {
          res = <React.Fragment key={key}>
            { this.codeLine(<>{`<${tag}`}{node.props}{">"}</>, indent) }
            { this.buildChildren(node.children, indent + 1) }
            { this.codeLine(`</${tag}>`, indent) }
          </React.Fragment>
        }
        break;
      default:
        res = null;
        break;
    }
    return res;
  }

  // Builds the inner HTML elements within the code lines
  // tag: HTML tag of the HTML element to be made
  // content: Content of the tag (String or JSX.Element only)
  // props: JSX.Element of all the props expanded out
  // close: Self closing or not
  private buildElement(
    tag: string, content?: React.ReactNode, props?: JSX.Element, close = false
  ) {
    // Don't try to understand... it's dank
    return <>
      {
        `<${tag}`}{props}{close? "/>" : <>{">"}{content? content: ""}{`</${tag}>`}</>
      }
    </>
  }

  // Turns React.ReactElement props into expanded JSX.Element props with children seperated
  private sortProps(el: React.ReactElement<any>) {
    const res: { props?: JSX.Element, children?: React.ReactNode } = {};
    res.props = <>
    {
      Object.keys(el).map((tag, tkey) => {
        let attr = el[tag];
        switch(tag) {
          case "className":
            tag = "class"; break;
          case "style":
            attr = this.getStyle(attr); break;
          case "children":
            res.children = attr; return;
          default:
            break;
        }
        key++;
        return <React.Fragment key={tkey}>&nbsp;
          <span className="code__tag">{tag}</span>
          ="
          <span className="code__attr">{attr}</span>
          "
        </React.Fragment>
      })
    }
    </>
    return res;
  }

  // Turns object interpretation of style into inline interpreation of style
  private getStyle(style: object) {
    let res = "";
    Object.keys(style).map(tag => {
      const attr = style[tag];
      res += ` ${tag.replace(/[A-Z]/g, (g) => "-" + g.toLowerCase())}: ${attr};`;
    });
    return res.slice(1);
  }

  // Wraps content in a code__line with indentation
  // content: content to put inside (string or JSX.Element)
  // indent: self-explainatory
  private codeLine(content: React.ReactNode, indent: number) {
    key++;
    return <li key={key} className="code__line">
      <span>{ TAB.repeat(indent) }</span>{ content }
    </li>
  }

  // Determines the type of children => string, array, object, or undefined/null
  private getType(children: React.ReactNode) {
    return Array.isArray(children)? "array": typeof(children)
  }

  // Blocks swipe when scrolling the code viewer
  private handleScroll() {
    clearTimeout(timeout);
    if (store.getState().swipe.enabled) {
      store.dispatch({ type: DISABLE_SWIPE });
    }
    timeout = setTimeout(() => {
      store.dispatch({ type: ENABLE_SWIPE });
    }, 200);
  }

}

export default Code;