import * as React from 'react';
import { meta } from '../../contentComponents';
import { nextComponent, toggleSidenav } from './../../redux/sidenavActions';
import { toggleSearch, setCurrentPage } from '../../redux/navActions';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

enum Pages {
  home = "Home",
  web = "Web",
  power = "Powerapps",
  ios = "iOS",
  android = "Android"
}

const DEFAULT_ITEMS = (() => {
  const res: IMetaState[] = [];
  const pages = Object.keys(meta);
  pages.map(i => {
    const section = meta[i];
    const items = Object.keys(section);
    items.map(j => {
      const item = section[j];
      res.push({ ...item, tag: j, page: i});
    })
  })
  return res;
})()

class Search extends React.Component<IProps, IState> {

  constructor(props: IProps) {
    super(props);
    this.state = {
      items: [],
      query: "",
      fade: false
    }

    this.filter = this.filter.bind(this);
    this.close = this.close.bind(this);
  }

  public shouldComponentUpdate(prevProps: IProps) {
    const { fade, show } = this.props;
    if (!show && show !== prevProps.show) {
      this.setState({fade: false, items: []});
      return false;
    } else if (fade !== prevProps.fade) {
      this.setState({fade: true});
      return false;
    }
    return true;
  }

  public render() {
    const { items, fade } = this.state;
    const { show } = this.props;
    return show?
    <div className={`search${fade? " search--hide": ""}`}>
      <input onChange={this.filter} className="search__input" type="text" placeholder="Search"/>
      <div className="search__wrapper--items">
        <ul className="search__items">
        {
          items.map(({ Title, page, tag }, key) => {
            return <li key={ key } onClick={this.goto.bind(this, tag, page)} className="search__item">
              <p className="search__title">{ Title }</p>
              <p className="search__desc">{ Pages[page] }</p>
            </li>
          })
        }
        </ul>
      </div>
      <div className="search__back" onClick={this.close}/>
    </div>:
    null
  }

  private goto(title: string, page: string) {
    const item = title !== this.props.item? title: "";
    const url = "/" + (page !== "home"? page: "");
    if (url !== this.props.page) {
      this.props.setCurrentPage(url, item, false, 400);
    } else if (title !== this.props.item) {
      this.props.nextComponent(item, 400);
    }
    this.close();
  }

  private close() {
    if (this.props.shift) {
      this.props.toggleSidenav();
    }
    this.props.toggleSearch(false, 400);
  }

  private filter(e: React.ChangeEvent<HTMLInputElement>) {
    const val = e.currentTarget.value;
    if (val !== "") {
      const items = DEFAULT_ITEMS.filter(({ keywords }) =>
        keywords.toString().indexOf(val) !== -1
      );
      this.setState({items});
    } else {
      this.setState({items: []});
    }
  }
}
const mapStateToProps = (state: any) => ({
  show: state.nav.search,
  fade: state.nav.fade_search,
  page: state.nav.active,
  item: state.sidenav.active,
  shift: state.sidenav.expanded
});
const mapDispatchToProps = (dispatch: any) => bindActionCreators({
  toggleSearch,
  nextComponent,
  setCurrentPage,
  toggleSidenav
}, dispatch)

export default connect<IStates, IDispatch>(mapStateToProps, mapDispatchToProps)(Search)

interface IMetaItem {
  Title: string,
  keywords: string[],
  noTitle?: boolean,
}

interface IMetaState extends IMetaItem { page: string; tag: string }

interface IStates {
  show: boolean;
  fade: boolean;
  page: string;
  item: string;
  shift: boolean;
}
interface IDispatch {
  toggleSearch: (state: boolean, delay: number) => (dispatch: any) => void;
  nextComponent: (component: string, delay: number) => (dispatch: any) => void;
  setCurrentPage: (active: string, component?: string, stay?: boolean, delay?: number) => (dispatch: any) => void;
  toggleSidenav: () => void;
}
interface IProps extends IStates, IDispatch { }
interface IState {
  items: IMetaState[];
  fade: boolean;
  query: "";
}