import * as React from 'react';
import error from '../../public/error.gif';

export default class Error extends React.Component<{className?: string}> {
  public render() {
    const { className } = this.props;
    return (
      <section className={"error" + (className? " " + className: "")}>
        <div className="error__card mlf__card">
          <img src={error} width="300"/>
          <h1 className="error__head">Error 404</h1>
          <p className="error__msg">A page you seek, yesssss.<br/>Here it is not, indeeeed.</p>
        </div>
      </section>
    )
  }
}
