import * as React from 'react';
import { MODAL_LEFT, MODAL_CENTER, MODAL_RIGHT } from '../../public/Modal';
import { connect } from 'react-redux'; // used for connecting to the redux store.
import { bindActionCreators } from 'redux';
import { SummonTheKawaiiSuperSaiyanRarePepe, GoodByeFwend, IncrementCounter, ResetCounter } from '../../redux/modalActions';

const EASTER_KEY = [38, 38, 40, 40, 37, 39, 37, 39, 66, 65];
const PEPE = [
  { type: "secondary", src: MODAL_LEFT, alt: "commoner pepe" },
  { type: "primary", src: MODAL_CENTER, alt: "classified rare pepe" },
  { type: "secondary", src: MODAL_RIGHT, alt: "commoner pepe" }
];

interface IProps extends IStateProps, IDispatchProps { }
class Modal extends React.Component<IProps> {
  constructor(props: IProps) {
    super(props);
    this.handleKeyDown = this.handleKeyDown.bind(this);
  }

  public componentDidMount() {
    document.addEventListener("keydown", this.handleKeyDown, {passive: true});
  }

  public componentWillUnmount() {
    document.removeEventListener("keydown", this.handleKeyDown);
  }

  public render() {
    const { summoned } = this.props;
    return (
      summoned?
      <div className='modal summoned'>
        <div className='modal-dialog' role='document'>
          <div className='modal-content'>
            <div className='modal-header'>
              <h5 className='modal-title'>You've summoned a super saiyan pepe and pepe henchmens, you'll be roughed up shortly...</h5>
            </div>
            <div className='modal__body'>
            {
              PEPE.map(({type, src, alt}, key) => 
                <img className={`modal__body__img--${type}`} src={src} alt={alt} key={key}/>
              )
            }
            </div>
            <div className='modal-footer'>
              <button type='button' className='btn btn-primary' onClick={this.props.GoodByeFwend}>
                rip
              </button>
            </div>
          </div>
        </div>
      </div>:
      null
    );
  }

  private handleKeyDown = (e: KeyboardEvent) => {
    const { KeyCounter } = this.props;
    if (e.keyCode === EASTER_KEY[KeyCounter? KeyCounter: 0]) {
      this.props.IncrementCounter();
    } else {
      this.props.ResetCounter();
    }

    if (KeyCounter === 9) {
      this.props.ResetCounter();
      this.props.SummonTheKawaiiSuperSaiyanRarePepe();
    }
  }
};

interface IStateProps {
  summoned: boolean;
  KeyCounter: number;
}
// Maps redux store state to this component's prop properties
const mapStateToProps = (state: any) => ({
  summoned: state.modal.summoned,
  KeyCounter: state.modal.KeyCounter
});

interface IDispatchProps {
  GoodByeFwend: () => void;
  SummonTheKawaiiSuperSaiyanRarePepe: () => void;
  IncrementCounter: () => void;
  ResetCounter: () => void;
}
// maps actions to props.
const mapDispatchToProps = (dispatch: any) => bindActionCreators({
  SummonTheKawaiiSuperSaiyanRarePepe,
  GoodByeFwend,
  IncrementCounter,
  ResetCounter
}, dispatch);

export default connect<IStateProps, IDispatchProps>(mapStateToProps, mapDispatchToProps)(Modal)