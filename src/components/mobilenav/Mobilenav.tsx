import * as React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { ROUTES } from '../../';
import { toggleMobilenav, setCurrentPage } from '../../redux/navActions';

interface IProps extends IStateProps, IDispatchProps { };

class Mobilenav extends React.Component<IProps, any> {

  constructor(props: IProps) {
    super(props);
    this.changePage = this.changePage.bind(this);
  }

  public changePage(url: string) {
    this.props.setCurrentPage(url, "", false, 400);
    this.props.toggleMobilenav();
  }

  public render() {
    const { expanded, active } = this.props;
    return <div className={`pos--clear mobile${expanded ? "--expanded" : "--collapsed"}`}>
      <div onClick={this.props.toggleMobilenav} className="mobile__backdrop" />
      <ul className="mlf-clear mobile__nav">
        <li className="mobile__nav__item--back">
          <h2 className="mobile__nav__header">Navigation</h2>
          <button className="mobile__nav__back" onClick={ this.props.toggleMobilenav }>
            <i className="material-icons mobile__nav__icon">close</i>Close
          </button>
        </li>
        <div className="mobile__wrapper--nav">
          {
            ROUTES.map(({ url, name }, key) => {
              return <li key={key}
                className={`mobile__nav__item${active === url? "--active": ""}`}
              >
                <a className="mobile__nav__link--clear" onClick={this.changePage.bind(this, url)}>
                  { name }
                </a>
              </li>
            })
          }
        </div>
      </ul>
    </div>;
  }
};

interface IStateProps {
  expanded: boolean;
  active: string
};
const mapStateToProps = (state: any) => ({
  expanded: state.nav.expanded,
  active: state.nav.active
});

interface IDispatchProps {
  toggleMobilenav: () => void;
  setCurrentPage: (path: string, component?: string, stay?: boolean, delay?: number) => void;
}
const mapDispatchToProps = (dispatch: any) => bindActionCreators({
  toggleMobilenav,
  setCurrentPage
}, dispatch);

export default connect<IStateProps, IDispatchProps>(mapStateToProps, mapDispatchToProps)(Mobilenav);