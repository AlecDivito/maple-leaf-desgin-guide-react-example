import { SET_COMPONENT, TOGGLE_SIDENAV, SET_COMPONENT_FADE } from './sidenavActions';
import { SWIPED_LEFT, SWIPED_RIGHT } from './swipeActions';

const CONTENT_DEFAULT_STATE = {
  active: "Overview",
  expanded: false,
  fade: false
}

export const sidenavReducer = (state = CONTENT_DEFAULT_STATE, action: any) => {
  switch (action.type) {
    case SET_COMPONENT_FADE:
      return { ...state, fade: true };
    case SET_COMPONENT:
      return { ...state, active: action.key, fade: false };
    case TOGGLE_SIDENAV:
      return { ...state, expanded: !state.expanded };
    case SWIPED_RIGHT:
      if (!state.expanded && !action.nav) {
        return ({ ...state, expanded: true });
      }
      return state;
    case SWIPED_LEFT:
      if (state.expanded && !action.nav) {
        return { ...state, expanded: false };
      }
      return state;
    default:
      return state;
  }
}