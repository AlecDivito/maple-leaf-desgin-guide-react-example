export const SET_COMPONENT_FADE = "SET_COMPONENT_FADE";
export const SET_COMPONENT = "SET_COMPONENT";
export const TOGGLE_SIDENAV = "TOGGLE_SIDENAV";

export function nextComponent(component: string, delay=0) {
  const key = (component === ""? "Overview" : component);
  location.hash = key === "Overview"? "": `#${key}`;
  return (dispatch: any) => {
    dispatch({ type: SET_COMPONENT_FADE, key });
    window.setTimeout(() => {
      dispatch({ type: SET_COMPONENT, key });
    }, delay);
  }
}

export function toggleSidenav() {
  return {
    type: TOGGLE_SIDENAV
  }
}