import { SUMMON_SUPER_SAIYAN_PEPE, BYE, INCREMENT_MATCH_COUNTER, RESET_MATCH_COUNTER } from './modalActions';

const CONTENT_DEFAULT_STATE = {
  summoned: false,
  KeyCounter: 0
}

export const modalReducer = (state = CONTENT_DEFAULT_STATE, action: any) => {
  switch (action.type) {
    case SUMMON_SUPER_SAIYAN_PEPE:
      return { ...state, summoned: true }
    case BYE:
      return { ...state, summoned: false }
    case INCREMENT_MATCH_COUNTER:
      return { ...state, KeyCounter: state.KeyCounter + 1 }
    case RESET_MATCH_COUNTER:
      return { ...state, KeyCounter: 0 }
    default:
      return state;
  }
}