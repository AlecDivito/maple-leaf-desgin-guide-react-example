export const SUMMON_SUPER_SAIYAN_PEPE = "SUMMON_SUPER_SAIYAN_PEPE";
export const BYE = "BYE";
export const INCREMENT_MATCH_COUNTER = "INCREMENT_MATCH_COUNTER";
export const RESET_MATCH_COUNTER = "RESET_MATCH_COUNTER";

export function SummonTheKawaiiSuperSaiyanRarePepe() {
  return {
    type: SUMMON_SUPER_SAIYAN_PEPE
  };
}
export function GoodByeFwend() {
  return {
    type: BYE
  };
}
export function IncrementCounter() {
  return {
    type: INCREMENT_MATCH_COUNTER
  };
}
export function ResetCounter() {
  return {
    type: RESET_MATCH_COUNTER
  };
}