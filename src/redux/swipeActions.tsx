export const SWIPED_LEFT = "SWIPED_LEFT";
export const SWIPED_RIGHT = "SWIPED_RIGHT";
export const SWIPED_UP = "SWIPED_UP";
export const SWIPED_DOWN = "SWIPED_DOWN";
export const DISABLE_SWIPE = "DISABLE_SWIPE";
export const ENABLE_SWIPE = "ENABLE_SWIPE";

export function onSwipingLeft(action: (dispatch: any, getState?: () => any) => void) {
  return action;
};

export function onSwipingRight(action: (dispatch: any, getState?: () => any) => void) {
  return action
};

export function onSwipingUp(action: (dispatch: any, getState?: () => any) => void) {
  return action
};

export function onSwipingDown(action: (dispatch: any, getState?: () => any) => void) {
  return action;
};