import { ENABLE_SWIPE, DISABLE_SWIPE } from './swipeActions';

const SWIPE_DEFAULT_STATE = {
    enabled: true
}

export const swipeReducer = (state = SWIPE_DEFAULT_STATE, action: any) => {
    switch(action.type) {
        case ENABLE_SWIPE:
            return { ...state, enabled: true };
        case DISABLE_SWIPE:
            return { ...state, enabled: false };
        default:
            return state;
    }
}