import { 
  TOGGLE_MOBILENAV, 
  SET_CURRENT_PAGE,
  TOGGLE_NAV,
  TOGGLE_SEARCH,
  FADE_SEARCH
} from './navActions';
import { SWIPED_LEFT, SWIPED_RIGHT } from './swipeActions';

const NAV_DEFAULT = {
  expanded: false,
  active: null,
  scrolled: false,
  search: false,
  fade_search: false
};

export const navReducer = (state = NAV_DEFAULT, action: any) => {
  switch (action.type) {
    case SWIPED_LEFT:
      if (!state.expanded && !action.sidenav) {
        return ({ ...state, expanded: true });
      }
      return state;
    case SWIPED_RIGHT:
      if (state.expanded && !action.sidenav) {
        return ({ ...state, expanded: false });
      }
      return state;
    case TOGGLE_MOBILENAV:
      return ({ ...state, expanded: !state.expanded });
    case SET_CURRENT_PAGE:
      return ({ ...state, active: action.active });
    case TOGGLE_NAV:
      return ({ ...state, scrolled: (action.state === undefined? !state.scrolled: action.state) });
    case TOGGLE_SEARCH:
      return ({ ...state, search: (action.state === undefined? !state.search: action.state) });
    case FADE_SEARCH:
      return ({ ...state, fade_search: !state.fade_search })
    default:
      return state;
  }
};