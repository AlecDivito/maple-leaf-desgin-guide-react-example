import { push } from "connected-react-router";

export const TOGGLE_MOBILENAV = "TOGGLE_MOBILENAV";
export const SET_CURRENT_PAGE = "SET_CURRENT_PAGE";
export const TOGGLE_SEARCH = "TOGGLE_SEARCH";
export const FADE_SEARCH = "FADE_SEARCH";
export const TOGGLE_NAV = "TOGGLE_NAV";

export function toggleMobilenav() {
  return {
    type: TOGGLE_MOBILENAV
  };
}

export function setCurrentPage(active: string, component?: string | null, stay?: boolean, delay = 0) {
  const page = active === "/home"? "": active;
  return (dispatch: any) => {
    dispatch({
      type: SET_CURRENT_PAGE,
      active: page,
    });
    if (!stay) {
      const location: { pathname: string, hash?: string } = { pathname: page };
      if (typeof(component) === "string") {
        location.hash = "#" + component
      }
      window.setTimeout(() => {
        dispatch(push(location));
      }, delay);
    }
  };
}

export function toggleNav(state?: boolean) {
  return {
    type: TOGGLE_NAV,
    state
  }
}

export function toggleSearch(state?: boolean, delay = 0) {
  return (dispatch: any) => {
    if (delay > 0) {
      dispatch({ type: FADE_SEARCH })
    }
    window.setTimeout(() => dispatch({
      type: TOGGLE_SEARCH,
      state
    }), delay);
  }
}