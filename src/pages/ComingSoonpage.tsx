import * as React from 'react';

export default class ComingSoonpage extends React.Component {
  public render() {
    return <section className="coming-soon">
    <div className="mlf__card coming-soon__card">
      <i className="material-icons coming-soon__gear" id="gear1">settings</i>
      <i className="material-icons coming-soon__gear" id="gear2">settings</i>
      <h1>Coming Soon</h1>
      <p>This page is in the works, keep an eye out for it!</p>
    </div>
    </section>
  }
}
