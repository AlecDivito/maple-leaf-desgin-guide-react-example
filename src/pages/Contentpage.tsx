import * as React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import Sidenav from '../components/sidenav/Sidenav';
import { toggleNav } from '../redux/navActions';
import Error from '../components/error/Error';
import AsyncComponent from '../components/async/AsyncComponent';
import { nextComponent } from '../redux/sidenavActions';

interface IProps extends IState, IDispatch {
  items: any;
  meta: any;
  history: any;
}
class Contentpage extends React.Component<IProps, any> {

  constructor(props: IProps) {
    super(props);
    this.state = {
      fadeout: false,
      mount: false,
      fade: true
    }
    this.scrollHandler = this.scrollHandler.bind(this);
  }

  public componentDidMount() {
    const hash = location.hash.slice(1);
    this.props.nextComponent(hash);
    setTimeout(() => this.setState({mount: true}), 10);
  }

  public componentDidUpdate(prevProps: IProps) {
    const { fade, fadeout } = this.props;
    if (fadeout !== prevProps.fadeout) {
      this.setState({fadeout: true});
    } else if (fade !== prevProps.fade) {
      this.setState({fade});
    }
  }

  public componentWillUnmount() {
    if (this.props.scrolled) {
      this.props.toggleNav(false);
    }
  }

  public render() {
    const { active, items, meta } = this.props;
    const { fadeout, fade, mount } = this.state;
    const item = meta[active];
    return (mount?
      <main onScroll={this.scrollHandler} className={`content${item && item.noTitle? " content--no-title": ""}`}>
        <Sidenav items={meta} fadeout={fadeout}/>
        {
          items && items[active]?
          <section className={"content__main" + (fadeout || fade? " content__main--fadeout": "")}>
            {
              meta[active].noTitle? null: <h1 className="content__h1">{ item.Title }</h1>
            }
            <AsyncComponent moduleProvider={items[active]} uid={active}/>
          </section>:
          <Error className="content__main" />
        }
      </main>:
    null)
  }

  private scrollHandler(e: any) {
    e.preventDefault();
    const { scrolled } = this.props;
    const Y = e.currentTarget.scrollTop;
    if (Y !== 0 && !scrolled || Y === 0 && scrolled) {
      this.props.toggleNav();
    }
  }
}

interface IState {
  active: string;
  fadeout: string;
  scrolled: boolean;
  fade: boolean;
}
const mapStateToProps = (state: any) => ({
  active: state.sidenav.active,
  fadeout: state.nav.active,
  scrolled: state.nav.scrolled,
  fade: state.sidenav.fade
});

interface IDispatch { 
  nextComponent: (key: string) => void;
  toggleNav: (state?: boolean) => { type: string, state?: boolean };
}
const mapDispatchToProps = (dispatch: any) => bindActionCreators({
  nextComponent,
  toggleNav
}, dispatch);

export default connect<IState, IDispatch>(mapStateToProps, mapDispatchToProps)(Contentpage);