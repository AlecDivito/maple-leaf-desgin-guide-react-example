import * as React from 'react';
import Error from '../components/error/Error';

export default class Errorpage extends React.Component {
  public render() {
    return <main className="error">
      <Error/>
    </main>
  }
}
