# Introduction
FancyHub, also known as DesignHub for the normies, is the fanciest webapp of all the hubs, within the fanciest organization which is fancyMLF.
Using the finest monkeys that $22 an hour can get, DesignHub provides an overview on the guidelines to provide a consistent and fancy frontend
across all applications within the organization.

**For even more information, bother our lord and savior Adam.**

![Lemon Grab](https://media1.tenor.com/images/61e979527421fff4ab8b5f07f55875ab/tenor.gif?itemid=3682545 =300x)

----
# Getting Started
1.	[Installation process](#installation)
2.	[Software dependencies](#dependencies)
3.	[Components](#components)
4.  [Contribute](#contribute)
# 

----
# <a id="installation"></a>Installation Process
Provided by [designhub-test.azurewebsites.net](https://designhub-test.azurewebsites.net).
### Production Version - Minified and Fancy
```
https://designhub-test.azurewebsites.net/css/mlfstyle.min.css
```
### Development Version
```
https://designhub-test.azurewebsites.net/css/mlfstyle.css
```

----
# <a id="dependencies"></a>Software Dependencies
## For using in applications
- You should have [**Bootstrap 4**](https://getbootstrap.com/docs/4.1/getting-started/) included on their application. Below we have the CDN's from their website for your convience (You're welcome c:)
```
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

<!-- All the scripts for Bootstrap -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
```

## For developing MLF Design Hub
- **FancyHub is dangerous and not something for mere mortals**.
It's fanciness can melt your mind and soul if you're not prepared physically and mentally.
It's dangerous to go alone, so here, to give you the highest odds of survival, take this *\*hands meme & instructions\** 
- You **MUST** have [**Node.js & npm**](https://nodejs.org/en/download/) installed on their machine. *(For installing Node.js on work computers, contact our other lord and savior, Shan, at Service Desk)*
- The **mystic runic command language** created back in the dark ages of MLF, also known as npm commands, to activate this world scale magic is as below (This might take a while)

### Magic circles to summon app
- Install the packages to run the application, this would create the ancient altar that houses FancyHub's soul
```
npm install
```
- Run the application (To re-run the application, you only need to do this step) and bring forth it's power
```
npm start
```
***WARNING:** We are not liable for any damages to you or MLF from this ritual. This includes, but not limited, loss of all senses, mental regression, and spontanous combustion*

## Node Dependencies (If your curious)
Dependencies help support the application and universe from total collapse as having both exist simutanously puts a lot of strain on the universe.
> *We lost a lot of parallel selfs making this. It was a one-in-multiple-millions future scenario*

All the dependencies should be in the package.json and automatically installed with *npm install*, but if curious about why...
- **Dependencies**
   - history, react-router-redux, react-redux, redux, redux-thunk
     - This is our router with redux integrated, redux is a our centralized state management system, redux-thunk allows us to delay dispatches (Make our actions async)
   - node-sass-chokidar
     - Handles turning our scss to css when running in dev and building using basically over half of all our package.json scripts
   - reactstrap
     - Allows for us to use javascript bootstrap components like tooltips to emulate using them normally in a ReactJS environment
- **Dev Dependencies**
   - npm-run-all
     - Basically lets us run scripts in series
   - sw-precache
     - Rewrites our serviceworker because the one in react basically blocks everything, making it impossible to expose the public folder

----
# <a id="components"></a>Components
- Button
- Checkbox
- Dropdown
- Input
- Radio
- Slider
- Switch
- Tooltip
- Herobrine

----
# <a id="contribute"></a>Contribute

This application is made in a way such that as little time and effort is required to integrate your new component with the application.
> That way people can focus on what matters - creating the component and documentations.

This serves as a **highly recommended way** of working with the application for people with little/no background to ReactJS or project. In no way is this set in stone. 

## Components
### Creation
-  All components should be created under the [src/formComponents](/src/formComponents/) folder.
-  The file name should properly reflect the type of UI component you are creating. (This would be your import name of your content page)
-  Note that the file name and the class should have the same name. (Camelcase & no spaces, ie. CoolTextarea, BananaButton, ...)
-  className is the kebab-case version of the Component. (ie BananaButton => banana-button)

Below is some starter code on creating you're first React component.
```
import * as React from 'react';

export class Input extends React.Component {
  public render() {
    return <div className="input">
      {/* Put your code in here */}
    </div>;
  }
};
```

### Integration with application
- Add export into barrel file, [src/formComponents/index.ts](src/formComponents/index.ts), for use elsewhere.
```
export * from './[YOUR COMPONENT NAME]';
```

## Content page for component
### Creation
-  All content pages should be created under the [src/contentComponents/[PAGE NAME]](/src/contentComponents) folder.
-  The file name should be the same as the file name of the created component.
-  The class name should be "Content". (This is for the asyncComponent)
-  **MAIN CSS STYLES FOR CONTENT**
   -  Main headers have the class "content__header". (With the first having "--first" appended to the end)
   -  Subheaders have class "content__subheader". (Usually in indented div with class "content__indent")

Below is some starter code on creating you're content page.
```
import * as React from 'react';

export class Content extends React.Component {
  public render() {
    return <div className="content__item content__item--[Block name of scss]">
      <div className="mlf__card mlf-clear">
        {/* Put your code in here */}
      </div>
    </div>;
  }
};
```

### Integration with application
-  Add your content page in the [src/contentComponents/[PAGE NAME]/index.tsx](/src/contentComponents) file.
-  Your content page should be imported as what your component is called. (We use dynamic imports for asyncComponent)
   - Async loading is to help increase performance, due to amount of content on each page, by chunking.

Below shows how you would add content to the file.
```
const A = () => import('./A');
const B = () => import('./B');
...
// const [Component Name] = () => import('./[Component Name]');  # ie. const Input = () => import('./Input'); 

export default {
  A,
  B,
  ... ,
  [Component Name]
};

// The key acts as the anchor of the component
// "Title" as the display name on sidenav and content page
// "hasTitle" to toggle between having a title or not
export const meta = {
  A: { Title: "Component A", hasTitle: [boolean] },
  B: { Title: "Component B" },
  ... ,
  [Component Name]: { Title: [Whatever you want it to be called], hasTitle: [Do you want title?] }
};
```

## SASS
### Creation
- The scss file for the component goes under the folder [src/scss/formComponents](/src/scss/formComponents).
- The scss file for the content page goes under the folder [src/scss/contentComponents](/src/scss/contentComponents).
- The component and content page scss file should have the same name as the component file.

### Integration
- Include the newly created files in the [src/scss/index.scss](/src/scss/index.scss) file.
   - If new file is a formComponent, then include it in [/src/scss/mlfstyle.scss] instead.
- They should sit under the appropriate commented area at the bottom of the file.
```
...

/* Page Stuff */
@import './pages/homepage.scss';
@import './pages/errorpage.scss';
@import './pages/contentpage.scss';

/* Components Stuff */
@import './components/nav/nav.scss';
@import './components/sidenav/sidenav.scss';
@import './components/error/error.scss';

/* Form Content Components Stuff */
@import './contentComponents/input.scss';
@import './contentComponents/overview.scss';
...
[ INSERT COMPONENT CONTENT PAGE IMPORT HERE ]

/* Form Content Components Stuff */
@import './contentComponents/input.scss';
@import './contentComponents/overview.scss';
...
[ INSERT COMPONENT CONTENT PAGE IMPORT HERE ]

...
```

----
Congratz, you made it to the end, have a meme

![Dank Meme](https://thumb.ibb.co/d1NLcy/Newfags_dont_know_what_brainlet_meme_is_c8d1da7dfc82a2081d8ec061fb18abb8.jpg)